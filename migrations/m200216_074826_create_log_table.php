<?php

use yii\db\Migration;

/**
 * Handles the creation of table `log`.
 */
class m200216_074826_create_log_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('log', [
            'id' => $this->primaryKey(),
            'created_at' => $this->timestamp()->defaultExpression('NOW()')->comment('Дата события'),
            'type' => $this->integer()->comment('Тип лога'),
            'log' => $this->text()->comment('Содержание'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('log');
    }
}
