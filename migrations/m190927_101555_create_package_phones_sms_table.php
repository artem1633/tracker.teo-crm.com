<?php

use yii\db\Migration;

/**
 * Handles the creation of table `package_phones_sms`.
 */
class m190927_101555_create_package_phones_sms_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('package_phones_sms', [
            'id' => $this->primaryKey(),
            'sms_id' => $this->string()->comment('Идентификатор сообщения'),
            'package_id' => $this->integer()->comment('Посылка'),
            'phone' => $this->string()->comment('Телефон'),
            'status' => $this->string()->comment('Статус'),
            'text' => $this->text()->comment('Текст сообщения'),
            'datetime_send' => $this->dateTime()->comment('Дата и время отправки'),
            'datetime_deliver' => $this->dateTime()->comment('Дата и время доставки'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-package_phones_sms-package_id',
            'package_phones_sms',
            'package_id'
        );

        $this->addForeignKey(
            'fk-package_phones_sms-package_id',
            'package_phones_sms',
            'package_id',
            'package',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-package_phones_sms-package_id',
            'package_phones_sms'
        );

        $this->dropIndex(
            'idx-package_phones_sms-package_id',
            'package_phones_sms'
        );

        $this->dropTable('package_phones_sms');
    }
}
