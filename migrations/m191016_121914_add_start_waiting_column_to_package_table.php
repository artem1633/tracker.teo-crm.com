<?php

use yii\db\Migration;

/**
 * Handles adding start_waiting to table `package`.
 */
class m191016_121914_add_start_waiting_column_to_package_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('package', 'start_waiting',
            $this->timestamp()->defaultValue(null)->comment('Дата начала ожидания вручения'));

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('package', 'start_waiting');
    }
}
