<?php

use yii\db\Migration;

/**
 * Handles adding outer_status to table `package_phones_sms`.
 */
class m191027_084820_add_outer_status_column_to_package_phones_sms_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('package_phones_sms', 'outer_status', $this->string()->comment('Статус сервиса отправки сообщений'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
