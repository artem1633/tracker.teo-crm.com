<?php

use yii\db\Migration;

/**
 * Class m191021_081830_insert_min_cost_call_to_settings_table
 */
class m191021_081830_insert_min_cost_call_to_settings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'min_cost_call',
            'value' => '0.8',
            'label' => 'Минимальная цена звонка, руб. (если цена звонка меньше указанной суммы, то звонок считается не завершенным)'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m191021_081830_insert_min_cost_call_to_settings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191021_081830_insert_min_cost_call_to_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
