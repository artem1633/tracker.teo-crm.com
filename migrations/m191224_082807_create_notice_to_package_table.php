<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notice_to_package`.
 */
class m191224_082807_create_notice_to_package_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('notice_to_package', [
            'id' => $this->primaryKey(),
            'notice_id' => $this->integer(),
            'package_id' => $this->integer(),
            'exec_date' => $this->timestamp()->defaultValue(null)->comment('Дата выполнения'),
            'last_exec_date' => $this->timestamp()->defaultValue(null)->comment('Дата последнего выполнения'),
            'status' => $this->string()->comment('Статус опопвещения'),
            'exec_num' => $this->integer()->comment('Кол-во выполненных оповещений'),
        ]);

        $this->addForeignKey(
            'fk-notice_to_package-notice_id',
            'notice_to_package',
            'notice_id',
            'notice',
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-notice_to_package-package_id',
            'notice_to_package',
            'package_id',
            'package',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-notice_to_package-notice_id', 'notice_to_package');
        $this->dropForeignKey('fk-notice_to_package-package_id', 'notice_to_package');
        $this->dropTable('notice_to_package');
    }
}
