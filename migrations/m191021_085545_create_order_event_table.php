<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_event`.
 */
class m191021_085545_create_order_event_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order_event', [
            'id' => $this->primaryKey(),
            'package_id' => $this->integer(),
            'number' => $this->string()->comment('Номер'),
            'date' => $this->timestamp()->defaultValue(null)->comment('Дата события'),
            'type' => $this->integer()->comment('Тип'),
            'name' => $this->string()->comment('Наименование'),
            'index_to' => $this->string()->comment('Индекс получателя'),
            'index' => $this->string()->comment('Индекс места события'),
            'sum' => $this->integer()->comment('Сумма наложенного платежа, коп.')
        ]);

        $this->addForeignKey('fk-order_eevent-package_id',
            'order_event',
            'package_id',
            'package',
            'id',
            'CASCADE',
            'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('order_event');
    }
}
