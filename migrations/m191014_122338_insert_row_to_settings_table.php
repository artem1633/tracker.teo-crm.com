<?php

use yii\db\Migration;

/**
 * Class m191014_122338_insert_row_to_settings_table
 */
class m191014_122338_insert_row_to_settings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'number_re_call', //Кол-во повторных звонков (если клиент не взял трубку)
            'label' => 'Кол-во повторных обзвонов',
            'value' => 3,
        ]);
        $this->insert('settings', [
            'key' => 'period_re_call', //Кол-во повторных звонков (если клиент не взял трубку)
            'label' => 'Интервал между повторными обзвонами (мин)',
            'value' => 30,
        ]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
       $this->delete('settings', ['key' => 'number_re_call']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191014_122338_insert_row_to_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
