<?php

use yii\db\Migration;

/**
 * Handles the creation of table `package_info`.
 */
class m191013_114113_create_package_info_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('package_info', [
            'id' => $this->primaryKey(),
            'package_id' => $this->integer()->comment('Идентификатор почтового отправления'),
            'type' => $this->string()->comment('Тип отправления'),
            'weight' => $this->double(2)->comment('Вес'),
            'cost' => $this->double(2)->comment('Объявленная ценность'),
            'pay_on_delivery' => $this->double(2)->comment('Наложенный платёж'),
        ]);

        $this->addForeignKey(
            'fk-package_info-package_id',
            'package_info',
            'package_id',
            'package',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-package_info-package_id', 'package_info');
        $this->dropTable('package_info');
    }
}
