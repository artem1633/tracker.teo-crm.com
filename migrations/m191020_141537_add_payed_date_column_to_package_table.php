<?php

use yii\db\Migration;

/**
 * Handles adding payed_date to table `package`.
 */
class m191020_141537_add_payed_date_column_to_package_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('package', 'payed_date', $this->timestamp()->defaultValue(null)->comment('Дата получения денег'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('package', 'payed_date');
    }
}
