<?php

use yii\db\Migration;

/**
 * Handles adding scheme_id to table `package`.
 */
class m191219_111540_add_scheme_id_column_to_package_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('package', 'scheme_id', $this->integer()->comment('Схема уведомлений'));

        $this->addForeignKey(
            'fk-package-scheme_id',
            'package',
            'scheme_id',
            'scheme',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-package-scheme_id','package');
        $this->dropColumn('package', 'scheme_id');
    }
}
