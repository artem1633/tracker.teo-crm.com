<?php

use yii\db\Migration;

/**
 * Handles the creation of table `scheme`.
 */
class m191217_104341_create_scheme_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('scheme', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'type' => $this->string()->comment('Тип [sms|call]'),
            'description' => $this->text()->comment('Описание'),
        ]);

        $this->batchInsert('scheme', ['name', 'type'], [
            ['СМС клиенту', 'sms'],
            ['Звонки клиенту', 'call'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('scheme');
    }
}
