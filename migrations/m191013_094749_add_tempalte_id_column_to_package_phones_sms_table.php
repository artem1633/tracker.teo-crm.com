<?php

use yii\db\Migration;

/**
 * Handles adding tempalte_id to table `package_phones_sms`.
 */
class m191013_094749_add_tempalte_id_column_to_package_phones_sms_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('package_phones_sms', 'template_id', $this->integer()->comment('Шаблон'));

        $this->addForeignKey(
            'fk-package_phones_sms-template_id',
            'package_phones_sms',
            'template_id',
            'sms_template',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-package_phones_sms-template_id','package_phones_sms');
        $this->dropColumn('package_phones_sms', 'template_id');
    }
}
