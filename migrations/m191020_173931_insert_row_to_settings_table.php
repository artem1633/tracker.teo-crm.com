<?php

use yii\db\Migration;

/**
 * Class m191020_173931_insert_row_to_settings_table
 */
class m191020_173931_insert_row_to_settings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'update_status_period',
            'value' => 10,
            'label' => 'Период обновления статусов отправлений (час)'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m191020_173931_insert_row_to_settings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191020_173931_insert_row_to_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
