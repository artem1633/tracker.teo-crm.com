<?php

use yii\db\Migration;

/**
 * Class m200312_134316_add_sms_sender_name_setting_to_settings_table
 */
class m200312_134316_add_sms_sender_name_setting_to_settings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%settings}}', ['key' => 'sms_sender_name' , 'value' => 'Magazin', 'label' => 'Имя отправителя СМС сообщения']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m200312_134316_add_sms_sender_name_setting_to_settings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200312_134316_add_sms_sender_name_setting_to_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
