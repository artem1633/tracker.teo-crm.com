<?php

use yii\db\Migration;

/**
 * Handles adding short_status to table `package`.
 */
class m191013_092509_add_short_status_column_to_package_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('package', 'short_status', $this->integer(2)->comment('Короткий статус'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('package', 'short_status');
    }
}
