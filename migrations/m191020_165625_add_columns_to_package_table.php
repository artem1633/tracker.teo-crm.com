<?php

use yii\db\Migration;

/**
 * Class m191020_165625_add_columns_to_package_info_table
 */
class m191020_165625_add_columns_to_package_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('package', 'reception_zip', $this->string()->comment('Индекс адресата при операции приема'));
        $this->addColumn('package', 'payment_zip', $this->string()->comment('Индекс операции при оплате'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m191020_165625_add_columns_to_package_info_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191020_165625_add_columns_to_package_info_table cannot be reverted.\n";

        return false;
    }
    */
}
