<?php

use yii\db\Migration;

/**
 * Handles adding destination_post_address to table `package`.
 */
class m191013_114112_add_columns2_to_package_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('package', 'recipient', $this->string()->comment('Получатель'));
        $this->addColumn('package', 'sender', $this->string()->comment('Отправитель'));
        $this->addColumn('package', 'destination_post_address', $this->string()->comment('Адрес ПО назначения'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('package', 'recipient');
        $this->dropColumn('package', 'sender');
        $this->dropColumn('package', 'destination_post_address');
    }
}
