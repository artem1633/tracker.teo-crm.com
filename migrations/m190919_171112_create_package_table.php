<?php

use yii\db\Migration;

/**
 * Handles the creation of table `package`.
 */
class m190919_171112_create_package_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('package', [
            'id' => $this->primaryKey(),
            'track_number' => $this->string()->comment('Трек-номер'),
            'send_date' => $this->date()->comment('Дата отправки'),
            'phone' => $this->string()->comment('Телефон'),
            'name' => $this->string()->comment('ФИО'),
            'sum' => $this->float()->comment('Сумма'),
            'product_title' => $this->string()->comment('Наименование товара'),
            'time_days' => $this->integer()->comment('Дней в пути'),
            'days_back' => $this->integer()->comment('Дней до начала возврата'),
            'status' => $this->string()->comment('Статус'),
            'last_status' => $this->string()->comment('Последний статус'),
            'payed' => $this->boolean()->defaultValue(false)->comment('Деньги получены'),
            'comment' => $this->text()->comment('Комменатрий'),
            'last_status_updated_at' => $this->dateTime()->comment('Дата и время последнего обновления по IP'),
            'created_at' => $this->dateTime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('package');
    }
}
