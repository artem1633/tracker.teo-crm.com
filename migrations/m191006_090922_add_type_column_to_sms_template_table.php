<?php

use app\models\SmsTemplate;
use yii\db\Migration;

/**
 * Handles adding type to table `sms_template`.
 */
class m191006_090922_add_type_column_to_sms_template_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('sms_template', 'type',
            $this->string()->defaultValue(SmsTemplate::TEMPLATE_TYPE_SMS)->comment('Тип шаблона сообщения (телефон, смс)'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('sms_template', 'type');
    }
}
