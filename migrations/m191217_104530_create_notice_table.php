<?php

use yii\db\Migration;

/**
 * Handles the creation of table `notice`.
 */
class m191217_104530_create_notice_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('notice', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'scheme_id' => $this->integer()->comment('Схема'),
            'template_id' => $this->integer()->comment('Шаблон сообщения/звонка'),
            'short_status' => $this->string()->comment('Короткий статус, при котором срабатывает обповещение'),
            'exec_date' => $this->timestamp()->defaultValue(null)->comment('Дата отправки оповещения'),
            'interval' => $this->integer()->comment('Интервал (дни)'),
            'repeat_num' => $this->smallInteger()->comment('Кол-во повторений'),
        ]);

        $this->addForeignKey(
            'fk-notice-scheme_id',
            'notice',
            'scheme_id',
            'scheme',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-notice-template_id',
            'notice',
            'template_id',
            'sms_template',
            'id',
            'SET NULL',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-notice-scheme_id', 'notice');
        $this->dropForeignKey('fk-notice-template_id', 'notice');
        $this->dropTable('notice');
    }
}
