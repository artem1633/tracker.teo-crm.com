<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m190919_172448_create_settings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->notNull()->unique()->comment('Ключ'),
            'value' => $this->string()->comment('Значение'),
            'label' => $this->text()->comment('Комментарий'),
        ]);
        $this->addCommentOnTable('settings', 'Настройки системы');

        $this->insert('settings', [
            'key' => 'sms_token',
            'value' => '',
            'label' => 'Токен для SMS',
        ]);
        $this->insert('settings', [
            'key' => 'sms_login',
            'value' => '',
            'label' => 'Логин для SMS',
        ]);
        $this->insert('settings', [
            'key' => 'sms_password',
            'value' => '',
            'label' => 'Пароль для SMS',
        ]);

        $this->insert('settings', [
            'key' => 'tel_token',
            'value' => '',
            'label' => 'Токен для Телефонии',
        ]);
        $this->insert('settings', [
            'key' => 'tel_login',
            'value' => '',
            'label' => 'Логин для Телефонии',
        ]);
        $this->insert('settings', [
            'key' => 'tel_password',
            'value' => '',
            'label' => 'Пароль для Телефонии',
        ]);

        $this->insert('settings', [
            'key' => 'mail_login',
            'value' => '',
            'label' => 'Логин для почты'
        ]);
        $this->insert('settings', [
            'key' => 'mail_password',
            'value' => '',
            'label' => 'Пароль для почты'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('settings');
    }
}
