<?php

use yii\db\Migration;

/**
 * Handles adding time_to_return to table `notice`.
 */
class m191219_092930_add_time_to_return_column_to_notice_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('notice', 'day_to_return', $this->integer()->comment('Дней до возврата'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('notice', 'day_to_return');
    }
}
