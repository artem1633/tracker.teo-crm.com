<?php

use yii\db\Migration;

/**
 * Class m191027_080847_add_columns_to_package_phones_sms_table
 */
class m191027_080847_add_cost_columns_to_package_phones_sms_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('package_phones_sms', 'cost', $this->double()->comment('Цена(руб)'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m191027_080847_add_columns_to_package_phones_sms_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191027_080847_add_columns_to_package_phones_sms_table cannot be reverted.\n";

        return false;
    }
    */
}
