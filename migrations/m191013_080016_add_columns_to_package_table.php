<?php

use yii\db\Migration;

/**
 * Handles adding destination_zip to table `package`.
 */
class m191013_080016_add_columns_to_package_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('package', 'destination_zip', $this->string()->comment('Индекс назначения'));
        $this->addColumn('package', 'date_acceptance_transfer', $this->timestamp()
            ->defaultValue(null)->comment('Дата приема перевода'));
        $this->addColumn('package', 'date_payment_transfer', $this->timestamp()
            ->defaultValue(null)->comment('Дата оплаты перевода'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('package', 'destination_zip');
        $this->dropColumn('package', 'date_acceptance_transfer');
        $this->dropColumn('package', 'date_payment_transfer');
    }
}
