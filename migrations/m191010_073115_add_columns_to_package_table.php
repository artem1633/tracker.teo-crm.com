<?php

use yii\db\Migration;

/**
 * Class m191010_073115_add_columns_to_package_table
 */
class m191010_073115_add_columns_to_package_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('package', 'zip', $this->string()->comment('Индекс'));
        $this->addColumn('package', 'post_address', $this->string()->comment('Адрес почтового отделения'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
       $this->dropColumn('package', 'zip');
       $this->dropColumn('package', 'post_address');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191010_073115_add_columns_to_package_table cannot be reverted.\n";

        return false;
    }
    */
}
