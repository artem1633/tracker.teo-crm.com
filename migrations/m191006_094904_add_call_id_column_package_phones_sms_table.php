<?php

use yii\db\Migration;

/**
 * Class m191006_094904_add_call_id_column_package_phones_sms_table
 */
class m191006_094904_add_call_id_column_package_phones_sms_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('package_phones_sms', 'call_id', $this->string()->comment('Идентификатор звонка'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m191006_094904_add_call_id_column_package_phones_sms_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191006_094904_add_call_id_column_package_phones_sms_table cannot be reverted.\n";

        return false;
    }
    */
}
