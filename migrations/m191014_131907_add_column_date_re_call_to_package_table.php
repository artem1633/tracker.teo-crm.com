<?php

use yii\db\Migration;

/**
 * Class m191014_131907_add_column_date_re_call_to_package_table
 */
class m191014_131907_add_column_date_re_call_to_package_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('package', 'date_re_call',
            $this->timestamp()->defaultValue(null)->comment('Дата последнего обзвона'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
       $this->dropColumn('package', 'date_re_call');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191014_131907_add_column_date_re_call_to_package_table cannot be reverted.\n";

        return false;
    }
    */
}
