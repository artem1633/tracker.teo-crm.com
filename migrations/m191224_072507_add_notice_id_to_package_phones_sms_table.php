<?php

use yii\db\Migration;

/**
 * Class m191224_072507_add_notice_id_to_package_phones_sms_table
 */
class m191224_072507_add_notice_id_to_package_phones_sms_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('package_phones_sms', 'notice_id', $this->integer()->comment('Идентификатор оповещения'));
        $this->addForeignKey(
            'fk-package_phones_sms-notice_id',
            'package_phones_sms',
            'notice_id',
            'notice',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
      $this->dropForeignKey('fk-package_phones_sms-notice_id', 'package_phones_sms');
      $this->dropColumn('package_phones_sms', 'notice_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191224_072507_add_notice_id_to_package_phones_sms_table cannot be reverted.\n";

        return false;
    }
    */
}
