<?php

use yii\db\Migration;

/**
 * Handles adding num_re_call_finished to table `package_phones_sms`.
 */
class m191014_124345_add_num_re_call_finished_column_to_package_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('package', 'num_re_call_finished',
            $this->integer(2)->defaultValue(0)->comment('Совершено повторных звонков'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
      $this->dropColumn('package', 'num_re_call_finished');
    }
}
