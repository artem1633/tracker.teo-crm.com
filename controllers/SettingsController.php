<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\Controller;
use app\models\Settings;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

//use yii\web\ForbiddenHttpException;

/**
 * SettingsController implements the CRUD actions for Users model.
 */
class SettingsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Отображет главную страницу настроек
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;

        $model = Yii::$app->user->identity;

        if ($request->isPost) {

            $data = $request->post();

            $model->load($request->post());
            $model->save();


            foreach ($data['Settings'] as $key => $value) {
                $setting = Settings::findByKey($key);

                if ($setting != null) {
                    $setting->value = $value;
                    $setting->save();
                    Yii::$app->session->setFlash('success', 'Настройки успешно сохранены');
                }
            }
        }
        $settings = Settings::find()->all();

        return $this->render('index', [
            'settings' => $settings,
            'model' => $model,
        ]);
    }
}
