<?php

namespace app\controllers;

use app\models\forms\MakeCallForm;
use app\models\forms\PackageImport;
use app\models\forms\SchemeForm;
use app\models\forms\SendSmsForm;
use app\models\Notice;
use app\models\NoticeToPackage;
use app\models\OrderEvent;
use app\models\PackageInfo;
use app\models\PackagePhonesSms;
use app\models\Scheme;
use app\models\Settings;
use app\services\PackageExcelExporter;
use app\services\RusMailSoap;
use kartik\grid\EditableColumnAction;
use Yii;
use app\models\Package;
use app\models\PackageSearch;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * PackageController implements the CRUD actions for Package model.
 */
class PackageController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => [
                    'notification-scheme',
                ],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'edit' => [
                'class' => EditableColumnAction::className(),
                'modelClass' => Package::className(),
                'outputValue' => function ($model, $attribute, $key, $index) {
                    if ($attribute == 'payed') {
                        if ($model->$attribute == 1) {
                            return 'Да';
                        } else {
                            return 'Нет';
                        }
                    }

                    return $model->$attribute;
                },
                'outputMessage' => function ($model, $attribute, $key, $index) {
                    return '';
                },
                'showModelErrors' => true,
            ]
        ]);
    }

    /**
     * Lists all Package models.
     * //     * @param string $show_empty_comment
     * //     * @param string $show_comment
     * @return mixed
     */
    public function actionIndex()
    {
        $get = Yii::$app->request->get('PackageSearch');

        $searchModel = new PackageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $count_row = Yii::$app->session->get('rows');

        if ($count_row) {
            $dataProvider->pagination->pageSize = $count_row;
        }
        $filter_state = Yii::$app->session->get('filter-state') ?? null;

        if (!$filter_state) {
            $filter_state = 'display: none;';
        }

        //Обработка значений фильтров
        $filters = [
            'date_period' => [
                'start' => $get['start'] ?? '',
                'end' => $get['end'] ?? '',
            ],
            'day_period' => [
                'start' => $get['init_day'] ?? '',
                'end' => $get['final_day'] ?? '',
            ],
            'show_comment' => $get['show_comment'] ?? 0 ? 'checked' : '',
            'show_empty_comment' => $get['show_empty_comment'] ?? 0 ? 'checked' : '',
        ];
        $sum = $dataProvider->query->sum('sum') ?? 0;
        Yii::info('Кол-во моделей: ' . count($dataProvider->models), 'test');
        Yii::info('Кол-во ключей: ' . count($dataProvider->getKeys()), 'test');

        $count = $dataProvider->getTotalCount();

        Yii::info('Кол-во найденных записей: ' . $count, 'test');
        Yii::info('Сумма найденных записей: ' . $sum, 'test');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'filters' => $filters,
            'count' => $count,
            'sum' => $sum,
            'filter_state' => $filter_state,
        ]);
    }

    /**
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function actionImport()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new PackageImport();

        if ($model->load($request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            $result_import = $model->upload();
            Yii::info($result_import, 'test');
            if ($result_import) {
                $errors = count($result_import['errors']) > 0 ? 'Ошибки импорта:<br>' . implode('<br>',
                        $result_import['errors']) : 'Ошибок импорта нет';
                return [
//                    'forceClose' => true,
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => 'Результаты импорта',
                    'content' => 'Всегод записей: ' . $result_import['total'] . '.<br>'
                        . 'Импортировано: ' . $result_import['saved'] . '.<br>'
                        . 'Пропущено: ' . $result_import['error'] . '.<br>'
                        . $errors,
                ];
            } else {
                return [
                    'title' => "Импорт",
                    'content' => $this->renderAjax('import', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Импортировать', ['class' => 'btn btn-primary', 'type' => "submit"]),
                ];
            }
        } else {
            return [
                'title' => "Импорт",
                'content' => $this->renderAjax('import', [
                    'model' => $model,
                ]),
                'footer' => Html::button('Начать импорт', [
                    'id' => 'start-import-btn',
                    'class' => 'btn btn-primary',
                    'type' => "submit",
                    'style' => 'display:none;'
                ]),
            ];
        }
    }

    /**
     * Экспорт выбранных отправлений в файл
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function actionExport()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $request = Yii::$app->request;

        if ($request->isPost) {
            $ids = $request->post('pks');
            $exporter = new PackageExcelExporter();
            $storagePath = Yii::getAlias('@webroot');
            $filename = $exporter->export($ids);
            Yii::info($storagePath, 'test');
            Yii::info($filename, 'test');

            $this->redirect('download-file?filename=' . $filename);
        }
    }

    /**
     * @param int $id
     * @throws NotFoundHttpException
     */
    public function actionOperationInfo($id)
    {
        $model = $this->findModel($id);
        $mailSoap = new RusMailSoap(Settings::findByKey('mail_login')->value,
            Settings::findByKey('mail_password')->value);
        $result = $mailSoap->getOperationHistory($model->track_number);

//        VarDumper::dump($result->OperationHistoryData->historyRecord, 10, true);
        Yii::info(json_decode(json_encode($result->OperationHistoryData->historyRecord), true), 'test');


    }

    /**
     * @param integer $id ID посылки
     * @return array
     */
    public function actionSendSms($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new SendSmsForm(['packageId' => $id]);

        if ($model->load($request->post())) {

            $send_result = $model->send();
            if (!$send_result['success']) {
                return [
                    'title' => "Отправка SMS",
                    'content' => 'Ошибка. <br>' . Json::encode($send_result['data']),
                    'footer' => Html::button('Закрыть',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }
            return [
                'forceClose' => true,
            ];
        } else {
            return [
                'title' => "SMS",
                'content' => $this->renderAjax('send-sms', [
                    'model' => $model,
                ]),
                'footer' => Html::button('Отправить', ['class' => 'btn btn-primary', 'type' => "submit"]),
            ];
        }
    }

    /**
     * Создает звонок адресату отправления
     * @param int $id Идентификатор почтового отправления
     * @return array
     */
    public function actionMakeCall($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;

        $package = Package::findOne($id) ?? null;

        if (!$package->preCheckCall()) {
            return [
                'title' => 'Ошибка создания звонка',
                'content' => $package->check_error,
            ];
        }

        $model = new MakeCallForm(['packageId' => $package->id]);

        if ($request->isPost && $model->load($request->post())) {
            $create_result = $model->make();
            if (!$create_result['success']) {
                return [
                    'title' => "Создание звонка",
                    'content' => '<p class="text-danger">Ошибка. <br>' . Json::encode($create_result['data']) . '</p>',
                    'footer' => Html::button('Закрыть',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }
            //После звонка записываем кол-во совершенных звонков адресату
//            $package->num_re_call_finished += 1;
//            $package->date_re_call = date('Y-m-d H:i:s', time());

            if (!$package->save()) {
                Yii::error($package->errors, '_error');
            }
            return [
                'forceClose' => true,
            ];
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'title' => "Создание звонка",
            'content' => $this->renderAjax('make-call', [
                'model' => $model,
            ]),
            'footer' => Html::button('Позвонить', ['class' => 'btn btn-primary', 'type' => "submit"]),
        ];
    }

    /**
     * Displays a single Package model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $rusMail = new RusMailSoap(Settings::findByKey('mail_login')->value,
            Settings::findByKey('mail_password')->value);
        $result = $rusMail->getOperationHistory($model->track_number);
        $dataProvider = new ArrayDataProvider([
            'allModels' => array_reverse(json_decode(json_encode($result->OperationHistoryData->historyRecord), true)),
        ]);

        $messageDataProvider = new ActiveDataProvider([
            'query' => PackagePhonesSms::find()
                ->andWhere(['package_id' => $model->id]),
        ]);

        $orderDataProvider = new ActiveDataProvider([
            'query' => OrderEvent::find()->andWhere(['package_id' => $model->id]),
            'sort' => [
                'defaultOrder' => ['date' => SORT_DESC]
            ]
        ]);

        Yii::info('Для отправления ' . $model->id . ' найдено платежных событий:' . $orderDataProvider->totalCount,
            'test');
        if ($orderDataProvider->totalCount == 0) {
            //Проверяем платежные события
            (new OrderEvent(['package_id' => $model->id]))->setEvents();
        }

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "посылка #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                    'dataProvider' => $dataProvider,
                    'messageDataProvider' => $messageDataProvider,
                ]),
                'footer' => Html::button('Отмена',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Изменить', ['update', 'id' => $id],
                        ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $model,
                'dataProvider' => $dataProvider,
                'messageDataProvider' => $messageDataProvider,
                'orderDataProvider' => $orderDataProvider,
            ]);
        }
    }

    /**
     * Creates a new Package model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Package();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Добавить посылку",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => "Добавить посылку",
                        'content' => '<span class="text-success">Создание посылки успешно завершено</span>',
                        'footer' => Html::button('ОК',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Создать еще', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                    ];
                } else {
                    return [
                        'title' => "Добавить посылку",
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Package model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Изменить посылку #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {

                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => "посылка #" . $id,
                        'content' => $this->renderAjax('view', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Изменить', ['update', 'id' => $id],
                                ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                    ];
                } else {
                    return [
                        'title' => "Изменить посылку #" . $id,
                        'content' => $this->renderAjax('update', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Package model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Package model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Проверяет и изменяет статус для выбранных посылок
     * @return mixed
     */
    public function actionBulkCheckStatus()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys

        Yii::info($pks, 'test');

        foreach ($pks as $pk) {
            $packageStatuses = Package::find()->andWhere(['id' => $pk])->all() ?? '';
            /** @var Package $package */
            foreach ($packageStatuses as $package) {
                $package->updateStatus();
            }
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['package']);
        }

    }

    /**
     * Finds the Package model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Package the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Package::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }

    /**
     * Проверка статуса посылки по кнопке "Обновить статусы отправлений" из списка посылок
     */
    public function actionCheckStatus()
    {
        set_time_limit(60 * 30);
        header("Cache-Control: no-cache");
        header("Content-Type: text/event-stream\n\n");
        $searchModel = new PackageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;

        Yii::info(Yii::$app->request->queryParams, 'test');
        Yii::info('Моделей - ' . count($dataProvider->models), 'test');
        Yii::info('Ключей - ' . count($dataProvider->keys), 'test');

        $total = $dataProvider->totalCount;

        Yii::info($total, 'test');
        $counter = 1;
        //Отсылаем кол-во обновляемых записей
        echo "event: progress\n";
        echo 'data:total=' . $total;
        echo "\n\n";
        flush();

        if ($total != 0) {
            /** @var Package $model */
            foreach ($dataProvider->models as $model) {
                $model->forceUpdate();
                Yii::info('Обновлено отправление: ' . $model->track_number, 'test');
                echo "event:progress\n";
                echo 'data:count=' . $counter;
                echo "\n\n";
                flush();
                $counter++;
            }
        } else {
            Yii::info('Нет обновляемых записей', 'test');
            echo "event:progress\n";
            echo 'data:count=0';
            echo "\n\n";
            flush();
        }

    }

    /**
     * Для теста замены тегов
     * @param int $id Идентификатор послыки
     *
     * @return string
     */
    public function actionTestReplace($id)
    {
        $model = Package::findOne($id) ?? null;
        if (!$model) {
            return 'посылка не найдена';
        }
        $model->text_to_process = '{ФИО}, Ваш заказ доставлен в Ваше почтовое отделение {Индекс} по адресу: {АдресОтделения}. Пожалуйста, получите его! Ваш номер РПО: {НомерРПО}.';
        VarDumper::dump($model->getProcessedText(Package::MESSAGE_TYPE_SMS), 10, true);
        return true;
    }

    /**
     * Отправка СМС выбранным клиентам
     * @return array
     */
    public function actionSendMultiplySms()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $request = Yii::$app->request;
        $model = new SendSmsForm();

        $pks = $request->post('pks') ?? null;
        $template_id = $request->post('SendSmsForm')['templateId'] ?? null;
        if ($pks) {
            Yii::$app->session->set('pks', $pks);
        }
        Yii::info($template_id, 'test');

        if ($template_id) {
            $ids = Yii::$app->session->get('pks') ?? null;
            Yii::$app->session->remove('pks');

            Yii::info($ids, 'test');
            /** @var int $id Package ID */
            foreach ($ids as $id) {
                $model->packageId = $id;
                $model->templateId = $template_id;
                $result = $model->send();
                Yii::info($result, 'test');
            }
            return [
                'forceReload' => '#crud-datatable',
                'forceClose' => true,
            ];
        } else {
            return [
                'title' => 'Выбор шаблона сообщения',
                'content' => $this->renderAjax('send-sms', [
                    'model' => $model,
                ]),
                'footer' => Html::button('Отправить', ['class' => 'btn btn-primary', 'type' => "submit"]),
            ];
        }
    }

    /**
     * Звонки выбранным клиентам
     * @return array
     */
    public function actionMakeMultiplyCall()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $request = Yii::$app->request;
        $model = new MakeCallForm();

        $pks = $request->post('pks') ?? null;
        $template_id = $request->post('MakeCallForm')['templateId'] ?? null;
        if ($pks) {
            Yii::$app->session->set('pks', $pks);
        }
        Yii::info($template_id, 'test');

        if ($template_id) {
            $ids = Yii::$app->session->get('pks') ?? null;
            Yii::$app->session->remove('pks');

            Yii::info($ids, 'test');
            /** @var int $id Package ID */
            foreach ($ids as $id) {
                $model->packageId = $id;
                $model->templateId = $template_id;
                $result = $model->make();
                Yii::info($result, 'test');
            }
            return [
                'forceReload' => '#crud-datatable',
                'forceClose' => true,
            ];
        } else {
            return [
                'title' => 'Выбор шаблона сообщения',
                'content' => $this->renderAjax('make-call', [
                    'model' => $model,
                ]),
                'footer' => Html::button('Отправить', ['class' => 'btn btn-primary', 'type' => "submit"]),
            ];
        }
    }

    /**
     * @param $filename
     * @return \yii\console\Response|Response
     */
    public function actionDownloadFile($filename)
    {
        $storagePath = Yii::getAlias('@webroot');
        Yii::info("$storagePath/$filename", 'test');
        return Yii::$app->response->sendFile("$storagePath/$filename", $filename);
    }

    /**
     * Для теста
     */
    public function actionGetPostalOrder()
    {
        $login = Settings::findByKey('mail_login')->value;
        $pass = Settings::findByKey('mail_password')->value;
        $client = new RusMailSoap($login, $pass);
//        Yii::info(implode('-', str_split('13424242342342344444444444234234', 3)), 'test');
//        return $client->getPostalOrderEvents('19000829274796', 'RUS');
        $client->getPostalOrderEvents('80088639949347', 'RUS');
//        VarDumper::dump(json_decode(json_encode($client->getOperationHistory('19000836224159')), true), 20, true);
    }

    /**
     * @param int $id Идентификатор посылки
     * @return array
     */
    public function actionInfo($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $package = Package::findOne($id) ?? null;

        /** @var PackageInfo $model */
        $model = PackageInfo::find()->andWhere(['package_id' => $id])->one() ?? null;
        $track_number = $package->track_number ?? null;

        if (!$model) {
            return [
                'title' => 'Ошибка',
                'content' => "Информация по отправлению $track_number не найдена",
                'footer' => ''
            ];
        }
        return [
            'title' => 'Информация об отправлении ' . $track_number,
            'content' => $this->renderAjax('_view-info', [
                'model' => $model,
                'pkg_model' => $package,
            ])
        ];
    }

    public function actionSetCountRow($count)
    {
        Yii::$app->session->set('rows', $count);
        return $this->redirect('/package/index');
    }

    public function actionFindByFilter()
    {
        Yii::info(Yii::$app->request->queryParams, 'test');
        $params = http_build_query(Yii::$app->request->queryParams);
        $this->redirect('/package/index?' . $params);
    }

    public function actionFindByBackDay($init, $final = 30)
    {
        $this->redirect(['/package/index', 'init_day' => $init, 'final_day' => $final]);
    }

    public function actionChangePayed($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = Package::findOne($id) ?? null;

        if (!$model) {
            return ['success' => 0, 'data' => 'Посылка не найдена'];
        }

        $model->payed = (int)!$model->payed;

        Yii::info($model->payed, 'test');


        if (!$model->save()) {
            Yii::error($model->errors, '_error');
            return ['success' => 0, 'data' => 'Ошибка сохранения'];
        }

        Yii::info('Дата оплаты изменена на ' . $model->payed_date, 'test');

        return [
            'success' => 1,
            'data' => $model->payed,
            'date_payed' => date('d M y H:i', strtotime($model->payed_date))
        ];
    }

    public function actionMultiplyComment()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new Package();

        $pks = $request->post('pks') ?? null;
        if ($pks) {
            Yii::$app->session->set('pks_multiply_comments', $pks);
        } else {
            $pks = Yii::$app->session->get('pks_multiply_comments');
            Yii::$app->session->remove('pks_multiply_comments');
        }

        if ($model->load($request->post())) {
            Yii::info($pks, 'test');

            /** @var int $package_id */
            foreach ($pks as $package_id) {
                $model_old = Package::findOne($package_id) ?? null;
                if ($model_old) {
                    if ($model_old->comment) {
                        $model_old->comment = $model_old->comment . '; ' . $model->comment;
                    } else {
                        $model_old->comment = $model->comment;
                    }
                    if (!$model_old->save()) {
                        Yii::error($model_old->errors, '_error');
                    }
                }
            }
            return [
                'forceReload' => '#crud-datatable',
                'forceClose' => true,
            ];
        } else {
            return [
                'title' => 'Комментарий',
                'content' => $this->renderAjax('multiply-comment', [
                    'model' => $model,
                ]),
                'footer' => Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"]),
            ];
        }

    }

    /**
     * Запись в сессию статуса окна фильтров (развернуто/свернуто)
     * @param string $state Статус (block/none)
     */
    public function actionSaveFilterState($state)
    {
        if ($state == 'none') {
            $state = 'block';
        } else {
            $state = 'none';
        }
        $state = 'display: ' . $state . ';';
        Yii::$app->session->set('filter-state', $state);
    }

    /**
     * Экспорт фильтрованных записей
     * @param string $start Начало периода
     * @param string $end Конец периода
     * @param string $init Дней до возврата мин.
     * @param string $finish
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function actionExportFilter(
        $start = '',
        $end = '',
        $init = '',
        $finish = ''
    ) {
        $searchModel = new PackageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (!$end) {
            $end = date('Y-m-d H:i:s', time());
        }

        if ($start && $end) {
            $start = date('Y-m-d 00:00:00', strtotime($start));
            $end = date('Y-m-d 23:59:59', strtotime($end));
            $dataProvider->query->andWhere(['BETWEEN', 'send_date', $start, $end]);
        }

        if ($init && $finish) {
            $dataProvider->query
                ->andWhere(['NOT IN', 'short_status', ['']])
                ->andWhere(['NOT', ['days_back' => null]])
                ->andWhere(['BETWEEN', 'days_back', $init, $finish]);
        }

        $pks = $dataProvider->query->select(['id'])->asArray()->all();

        $exporter = new PackageExcelExporter();
        $storagePath = Yii::getAlias('@webroot');
        $filename = $exporter->export($pks);
        Yii::info($storagePath, 'test');
        Yii::info($filename, 'test');

        $this->redirect('/package/download-file?filename=' . $filename);

    }

    /**
     * Отображение формы, установка или сброс схемы оповещения
     * @throws \yii\db\Exception
     */
    public function actionNotificationScheme()
    {
        set_time_limit(900);

        $request = Yii::$app->request;
        $model = new SchemeForm();

        $pks = $request->post('pks') ?? null;
        $scheme_id = $request->post('SchemeForm')['scheme_id'] ?? null;

        if ($pks) {
            Yii::$app->session->set('pks_notify', $pks);
            Yii::$app->session->remove('pks');
        }

        if ($scheme_id) {
            $pks = Yii::$app->session->get('pks_notify');
            Yii::$app->session->remove('pks_notify');

            /** @var int $id ID РПО */
            foreach ($pks as $id) {
                $package = Package::findOne($id) ?? null;

                if (!$package) {
                    continue;
                }

                //Если для РПО схема уже назначена - пропускаем
                if ($package->scheme_id) {
                    Yii::warning('Для РПО ' . $package->track_number . ' уже назначена схема. Проупускаем.');
                    continue;
                }

                $package->scheme_id = $scheme_id;

                //Генерим записи для связи package - notice
                $scheme_model = Scheme::findOne($scheme_id) ?? null;
                /** @var Notice[] $notices */
                $notices = $scheme_model->notices;

                $db = \Yii::$app->db;
                $transaction = $db->beginTransaction();

                Yii::info(count($notices), 'test');

                foreach ($notices as $notice) {
                    Yii::info($notice->getAttributes(), 'test');

                    $exist = NoticeToPackage::find()
                        ->andWhere(['notice_id' => $notice->id])
                        ->andWhere(['package_id' => $package->id])
                        ->exists();

                    if ($exist) {
                        //Если для посылки уже назначено данное уведомление - пропускаем добавление
                        Yii::warning('для РПО ' . $package->track_number . ' уже назначено уведомление ' . $notice->name);
                        $transaction->rollback();
                        continue;
                    }

                    $ntp = new NoticeToPackage();
                    $ntp->status = NoticeToPackage::STATUS_TO_WORK;
                    $ntp->notice_id = $notice->id;
                    $ntp->package_id = $package->id;

                    if (!$ntp->save()) {
                        Yii::error($ntp->errors, '_error');
                    }
                }
                if (!$package->save()) {
                    $transaction->rollback();
                    Yii::error($package->errors, '_error');
                }
                $transaction->commit();

            }

            return [
                'forceClose' => true,
                'forceReload' => '#crud-datatable-pjax'
            ];
        }

        return [
            'title' => 'Выбор схемы уведомления',
            'content' => $this->renderAjax('_scheme_form', [
                'model' => $model,
            ]),
            'footer' => Html::submitButton('Назначить выбранную схему', [
                'class' => 'btn btn-success',
            ])
        ];

    }

    /**
     * Удаляет схемы оповещения у отправлений
     * @return array
     * @throws \Throwable
     */
    public function actionDelScheme()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $package_ids = Yii::$app->session->get('pks_notify');
        Yii::$app->session->remove('pks_notify');

        return (new Package())->removeSchemes($package_ids);
    }

    /**
     * Удаляет комментарии у отправлений
     * @return array
     * @throws \Throwable
     */
    public function actionDelComment()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $package_ids = Yii::$app->session->get('pks_multiply_comments');
        Yii::$app->session->remove('pks_multiply_comments');

        return (new Package())->removeComments($package_ids);
    }
}
