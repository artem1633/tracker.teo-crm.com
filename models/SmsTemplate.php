<?php

namespace app\models;

use app\components\helpers\TagHelper;
use yii\base\Component;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "sms_template".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $content Текст
 * @property string $created_at
 * @property string $type

 */
class SmsTemplate extends ActiveRecord
{

    const TEMPLATE_TYPE_SMS = 'sms';
    const TEMPLATE_TYPE_CALL = 'call';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sms_template';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'content'], 'required'],
            [['content'], 'string'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'content' => 'Текст',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @param Component $model
     * @return string
     */
    public function apply($model)
    {
        return TagHelper::handleModel($this->content, $model);
    }

    /**
     * Получает список шаблонов для сообщений по типу (для звонков или для смс)
     * @param string $type Тип шаблона
     * @return array
     */
    public static function getList($type = self::TEMPLATE_TYPE_SMS)
    {
        return ArrayHelper::map(self::find()->andWhere(['type' => $type])->all(), 'id', 'name');
    }

    /**
     * Возвращает список типов шаблонов сообщений
     * @return array
     */
    public static function getTypeList()
    {
        return [self::TEMPLATE_TYPE_SMS => 'СМС', self::TEMPLATE_TYPE_CALL => 'Звонок'];
    }

    public static function getAllTemplates()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }


}
