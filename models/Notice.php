<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "notice".
 *
 * @property int $id
 * @property int $scheme_id Схема
 * @property int $template_id Шаблон сообщения/звонка
 * @property string $short_status Короткий статус, при котором срабатывает обповещение
 * @property string $exec_date Дата отправки оповещения
 * @property int $interval Интервал (дни)
 * @property int $repeat_num Кол-во повторений
 * @property int $name Наименование
 * @property int $day_to_return Дней до возврата
 * @property string $type Тип (звонок/смс)
 *
 * @property Scheme $scheme
 * @property SmsTemplate $template
 */
class Notice extends ActiveRecord
{
    /** @var string */
    public $type;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['scheme_id', 'template_id', 'interval', 'repeat_num', 'day_to_return'], 'integer'],
            [['exec_date'], 'safe'],
            [['short_status', 'name'], 'string', 'max' => 255],
            [['scheme_id'], 'exist', 'skipOnError' => true, 'targetClass' => Scheme::className(), 'targetAttribute' => ['scheme_id' => 'id']],
            [['template_id'], 'exist', 'skipOnError' => true, 'targetClass' => SmsTemplate::className(), 'targetAttribute' => ['template_id' => 'id']],
            [['name'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'scheme_id' => 'Схема',
            'template_id' => 'Шаблон уведомления',
            'short_status' => 'Короткий статус, при котором срабатывает обповещение',
            'exec_date' => 'Дата отправки оповещения',
            'interval' => 'Интервал (дни)',
            'repeat_num' => 'Кол-во повторений',
            'day_to_return' => 'Дней до возврата',
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
   {
       parent::afterSave($insert, $changedAttributes);

       if ($insert){
           //Ищем все РПО со схемой в которую входит добавляемое оповещение
           $packages = Package::find()->andWhere(['scheme_id' => $this->scheme_id])->all();

           //К найденым РПО добавляем связь
           /** @var Package $package */
           foreach ($packages as $package){
               $model = new NoticeToPackage([
                   'notice_id' => $this->id,
                   'package_id' => $package->id,
                   'status' => 'work'
                ]);

               if (!$model->save()){
                   \Yii::error($model->errors, '_error');
               }
           }
       }
   }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScheme()
    {
        return $this->hasOne(Scheme::className(), ['id' => 'scheme_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(SmsTemplate::className(), ['id' => 'template_id']);
    }

    public function getNoticeToPackage()
    {
        return $this->hasMany(NoticeToPackage::className(), ['notice_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\query\NoticeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\NoticeQuery(get_called_class());
    }
}
