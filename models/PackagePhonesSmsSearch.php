<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PackagePhonesSmsSearch represents the model behind the search form about `app\models\PackagePhonesSms`.
 */
class PackagePhonesSmsSearch extends PackagePhonesSms
{
    public $type;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'package_id'], 'integer'],
            [
                [
                    'sms_id',
                    'call_id',
                    'phone',
                    'status',
                    'text',
                    'datetime_send',
                    'datetime_deliver',
                    'created_at',
                    'type',
                    'message_id'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PackagePhonesSms::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'package_id' => $this->package_id,
            'datetime_send' => $this->datetime_send,
            'datetime_deliver' => $this->datetime_deliver,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'sms_id', $this->sms_id])
            ->andFilterWhere(['like', 'call_id', $this->call_id])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'status', $this->status])
            ->orFilterWhere(['like', 'sms_id', $this->message_id])
            ->orFilterWhere(['like', 'call_id', $this->message_id])
            ->andFilterWhere(['like', 'text', $this->text]);

        if ($this->type == SmsTemplate::TEMPLATE_TYPE_SMS) {
            $query->andWhere(['IS', 'call_id', null]);
        } elseif ($this->type == SmsTemplate::TEMPLATE_TYPE_CALL) {
            $query->andWhere(['IS', 'sms_id', null]);
        }
        return $dataProvider;
    }
}
