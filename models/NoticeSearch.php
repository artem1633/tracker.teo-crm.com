<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * NoticeSearch represents the model behind the search form about `app\models\Notice`.
 */
class NoticeSearch extends Notice
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'scheme_id', 'template_id', 'interval', 'repeat_num', 'day_to_return'], 'integer'],
            [['short_status', 'exec_date', 'name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Notice::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'scheme_id' => $this->scheme_id,
            'template_id' => $this->template_id,
            'exec_date' => $this->exec_date,
            'interval' => $this->interval,
            'repeat_num' => $this->repeat_num,
            'day_to_return' => $this->day_to_return,
        ]);

        $query->andFilterWhere(['like', 'short_status', $this->short_status]);
        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
