<?php

namespace app\models\forms;

use app\models\PackagePhonesSms;
use app\models\SmsTemplate;
use Yii;
use app\models\Package;
use yii\base\Model;
use yii\helpers\Json;

/**
 * Class SendSmsForm
 * @property int $packageId
 * @property int $templateId
 * @package app\models\forms
 */
class SendSmsForm extends Model
{
    /**
     * @var int
     */
    public $packageId;

    /**
     * @var int
     */
    public $templateId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['packageId', 'templateId'], 'required'],
        ];
    }

    public function send()
    {
        $package = Package::findOne($this->packageId) ?? null;
        $template = SmsTemplate::findOne($this->templateId) ?? null;
        $data = [];

        if($package == null || $template == null){
            return false;
        }
        Yii::info($package->toArray(), 'test');

        $smsText = $template->apply($package);

        //Обрабатываем тэги в тексте
        $package->text_to_process = $smsText;
        $smsText = $package->getProcessedText(Package::MESSAGE_TYPE_SMS);
        Yii::info('Обработнанный текст: ' . $smsText, 'test');

        $response = Yii::$app->smsService->send($package->phone, $smsText);

        Yii::info($response, 'test');

        $response = Json::decode($response);

        if ($response['result'] == 'reject'){
            Yii::error($response['reason'], '_error');
            $success = 0;
            $data['error_send_sms'] = $response['reason'];
        } else {
            $model = new PackagePhonesSms([
                'sms_id' => $response['id'],
                'package_id' => $package->id,
                'text' => $smsText,
                'phone' => $package->phone,
                'template_id' => $template->id,
            ]);

            if (!$model->save(false)){
                Yii::error($model->errors, '_error');
                $success = 0;
                $data['save_errors'] = $model->errors;
            } else {
                $success = 1;
            }
        }

        return ['success' => $success, 'data' => $data];

    }
}