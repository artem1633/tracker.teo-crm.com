<?php

namespace app\models\forms;

use app\services\PackageExcelImporter;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class PackageImport
 * @package app\models\forms
 */
class PackageImport extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;

    /**
     * @var PackageExcelImporter
     */
    public $importer;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['file'], 'required'],
            [['file'], 'file', 'extensions' => 'xls, xlsx']
        ];
    }

    /**
     * @return array|bool
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function upload()
    {
        set_time_limit(60 * 10);

        if($this->validate()){
            if(is_dir('tmp') == false){
                mkdir('tmp');
            }

            $path = "tmp/{$this->file->baseName}.{$this->file->extension}";

            $this->file->saveAs($path);

            $importer = new PackageExcelImporter(['sourceFile' => $path]);
            $importer->import();

            $this->importer = $importer;

            return [
                'total' => $importer->totalCount,
                'saved' => $importer->savedCount,
                'error' => $importer->errorCount,
                'errors' => $importer->errors,
            ];
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'file' => 'Файл'
        ];
    }
}