<?php

namespace app\models\forms;

use app\models\Scheme;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class SchemeForm
 * @property array $packages_id
 * @package app\models\forms
 */
class SchemeForm extends Model
{
    /**
     * @var array
     */
    public $scheme_id;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['packages_id'], 'required'],
        ];
    }

    public function getSchemeList()
    {
        return ArrayHelper::map(Scheme::find()->all(), 'id', 'name');
    }

}