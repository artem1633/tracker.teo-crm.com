<?php

namespace app\models\forms;

use app\models\PackagePhonesSms;
use app\models\SmsTemplate;
use Yii;
use app\models\Package;
use yii\base\Model;
use yii\helpers\Json;

/**
 * Class SendSmsForm
 * @package app\models\forms
 */
class MakeCallForm extends Model
{
    /**
     * @var int
     */
    public $packageId;

    /**
     * @var int
     */
    public $templateId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['packageId', 'templateId'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'packageId' => 'Почтовое отправление',
            'templateId' => 'Шаблон',
        ];
    }


    /**
     * Добавление звонка
     *
     *
     * @return array|bool
     * ВОзвращает массив
     * [
     *  'success' => <0 - неудача или 1 - успех>,
     *  'data' =>
     *      [
     *          'error' => 'Если есть, то ошибки при сохранении звонка. Если нет - то элемент отсутствует',
     *          'message' => 'Сообщение при успешном выполнении опеации, при ошибке элемент отсутствует'
     *      ]
     * ]
     */
    public function make()
    {
        $package = Package::findOne($this->packageId);
        $template = SmsTemplate::findOne($this->templateId);
        $data = [];

        if ($package == null || $template == null) {
            return false;
        }
        Yii::info($package->toArray(), 'test');

        $callText = $template->apply($package);
        //Обрабатываем тэги в тексте
        $package->text_to_process = $callText;
        $callText = $package->getProcessedText(Package::MESSAGE_TYPE_CALL);

        $response = Yii::$app->voiceService->send($package->phone, $callText);

        Yii::info($response, 'test');

        $response = Json::decode($response);

        Yii::info($response, 'test');

        if ($response['status'] == 'success') {
            $model = new PackagePhonesSms([
                'call_id' => $response['data'][0]['id'],
                'package_id' => $package->id,
                'text' => $callText,
                'phone' => $package->phone,
                'created_at' => date('Y-m-d H:i:s', $response['data'][0]['createdAt']),
                'template_id' => $template->id,
            ]);
            if (!$model->save(false)) {
                Yii::error($model->errors, '_error');
                $success = 0;
                $data['error'] = $model->errors;
            } else {
                $success = 1;
                $data['message'] = 'Create success';
            }
        } else {
            Yii::error($response, '_error');
            $success = 0;
            $data['error'] = $response['data']['message'];
        }

        return ['success' => $success, 'data' => $data];

    }

}