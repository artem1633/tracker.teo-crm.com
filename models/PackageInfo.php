<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "package_info".
 *
 * @property int $id
 * @property int $package_id Идентификатор почтового отправления
 * @property string $type Тип отправления
 * @property double $weight Вес
 * @property double $cost Объявленная ценность
 * @property double $pay_on_delivery Наложенный платёж
 *
 * @property Package $package
 */
class PackageInfo extends ActiveRecord
{
    public $sender;
    public $recipient;
    public $index_recipient;
    public $address_recipient;
    public $days_back;
    public $time_days;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'package_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['package_id'], 'integer'],
            [['weight', 'cost', 'pay_on_delivery'], 'number'],
            [['type'], 'string', 'max' => 255],
            [['package_id'], 'exist', 'skipOnError' => true, 'targetClass' => Package::className(), 'targetAttribute' => ['package_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'package_id' => 'Идентификатор почтового отправления',
            'type' => 'Тип отправления',
            'weight' => 'Вес',
            'cost' => 'Объявленная ценность',
            'pay_on_delivery' => 'Наложенный платёж',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackage()
    {
        return $this->hasOne(Package::className(), ['id' => 'package_id']);
    }


    /**
     * @param \stdClass $history История посылки
     */
    public function setMass($history): void
    {
        /** @var \stdClass $item */
        foreach ($history as $history_item){
            $item_param = $history_item['ItemParameters'];

            if (isset($item_param['Mass'])){
                $this->weight = $item_param['Mass'];
//                \Yii::info('Масса: ' . $this->weight, 'test');
                break;
            }
        }
    }
}
