<?php

namespace app\models;

use app\services\RusMailSoap;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "package".
 *
 * @property int $id
 * @property string $track_number Трек-номер
 * @property string $send_date Дата отправки
 * @property string $phone Телефон
 * @property string $name ФИО
 * @property double $sum Сумма
 * @property string $product_title Наименование товара
 * @property int $time_days Дней в пути
 * @property int $days_back Дней до начала возврата
 * @property string $status Статус
 * @property string $last_status Последний статус
 * @property int $payed Деньги получены
 * @property string $comment Комменатрий
 * @property string $last_status_updated_at Дата и время последнего обновления по IP
 * @property string $created_at
 * @property string $text_to_process Текст с тэгами
 * @property string $zip Индекс
 * @property string $post_address Адрес почтового отделения
 * @property string $destination_zip Индекс назначения(получателя)
 * @property string $date_acceptance_transfer Дата приема перевода
 * @property string $date_payment_transfer Дата оплаты перевода
 * @property int $short_status Короткий статус
 * @property string $destination_post_address Адрес назначения (почтового отделения)
 * @property string $recipient Получатель
 * @property string $sender Отправитель
 * @property string $num_re_call_finished Количество совершенных повторных звонков
 * @property string $date_re_call Дата последнего звонка
 * @property string $check_error Ошибки проверки перед созданием звонка
 * @property string $start_waiting Дата начала ожидания вручения
 * @property string $payed_date Дата получения денег
 * @property string $start Начальная дата периода отправки
 * @property string $end Клонечная дата периода отправки
 * @property string $init_day Начальное кол-во дней до отправки
 * @property string $final_day Конечное кол-во дней до отправки
 * @property string $show_comment Для фильтра показа/скрытия комментов
 * @property string $show_empty_comment Для фильтра показа/скрытия комментов
 * @property string $sender_zip Индекс отправителя
 * @property integer $operation_id Идентификатор операции
 * @property array $_history История отправлений
 * @property int $scheme_id Схема уведомлений
 *
 * @property PackageInfo $info
 * @property OrderEvent[] $orderEvents
 * @property Scheme $scheme
 * @property Notice[] $notices
 * @property NoticeToPackage[] $noticesToPackage
 */
class Package extends ActiveRecord
{
    const MESSAGE_TYPE_SMS = 'sms';
    const MESSAGE_TYPE_CALL = 'call';

    /**
     * @var string
     */
    public $text_to_process;

    /** @var string */
    public $check_error;

    public $start, $end, $init_day, $final_day;

    public $show_comment; //Для фильтра показа/скрытия комментов
    public $show_empty_comment; //Для фильтра показа/скрытия комментов

    public $sender_zip;
    public $operation_id;

    /** @var array */
    private $_history;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'package';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['track_number'], 'required'],
            ['track_number', 'unique'],
            [
                [
                    'send_date',
                    'last_status_updated_at',
                    'created_at',
                    'date_acceptance_transfer',
                    'date_payment_transfer',
                    'start_waiting',
                    'payed_date',
                    'start',
                    'end',
                    'init_day',
                    'final_day',
                ],
                'safe'
            ],
            [['sum'], 'number'],
            [['time_days', 'days_back', 'payed', 'short_status', 'num_re_call_finished', 'scheme_id'], 'integer'],
            [['comment', 'destination_post_address', 'recipient', 'sender', 'date_re_call'], 'string'],
            [
                ['track_number', 'phone', 'name', 'product_title', 'status', 'last_status', 'destination_zip'],
                'string',
                'max' => 255
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'track_number' => '№ РПО',
            'send_date' => 'Дата отправки',
            'phone' => 'Телефон',
            'name' => 'ФИО',
            'sum' => 'Сумма',
            'product_title' => 'Наименование товара',
            'time_days' => 'Дней в пути',
            'days_back' => 'Дней до начала возврата',
            'status' => 'Статус',
            'last_status' => 'Последний статус',
            'payed' => 'Деньги получены',
            'comment' => 'Комменатрий',
            'last_status_updated_at' => 'Дата обновления статуса',
            'created_at' => 'Дата и время создания',
            'destination_zip' => 'Индекс получателя',
            'date_acceptance_transfer' => 'Дата приема перевода',
            'date_payment_transfer' => 'Дата оплаты перевода',
            'short_status' => 'Статус', //Короткий статус
            'destination_post_address' => 'Адрес ПО назначения',
            'recipient' => 'Получатель',
            'sender' => 'Отправитель',
            'num_re_call_finished' => 'Кол-во совершенных повторных звонков',
            'date_re_call' => 'Дата последнего звонка',
            'start_waiting' => 'Дата начала ожидания',
            'payed_date' => 'Дата получения денег',
            'scheme_id' => 'Схема уведомлений',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->phone) {
            $this->phone = str_replace('-', '', $this->phone);
        }

        if ($this->payed && !$this->payed_date) {
            $this->payed_date = date('Y-m-d H:i:s', time());
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        //Сразу же проверяем статус нового отправления
        if ($insert) {
            $this->updateStatus();
        }

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInfo()
    {
        return $this->hasOne(PackageInfo::className(), ['package_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(OrderEvent::className(), ['package_id', 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScheme()
    {
        return Scheme::find()->andWhere(['id' => $this->scheme_id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotices()
    {
        return Notice::find()
            ->joinWith(['noticeToPackage ntp'])
            ->andWhere(['ntp.package_id' => $this->id]);
    }

    public function getNoticesToPackage()
    {
        return $this->hasMany(NoticeToPackage::className(), ['package_id' => 'id']);
    }

    /**
     * Обновляет статус почтового отправления
     * @return boolean
     */
    public function updateStatus()
    {
        $login = Yii::$app->session->get('pr_login');
        $password = Yii::$app->session->get('pr_pass');

        if (!$login || !$password) {
            $login = Settings::findByKey('mail_login')->value;
            $password = Settings::findByKey('mail_password')->value;
        }

        $mailSoap = new RusMailSoap($login, $password);
        $result = json_decode(json_encode($mailSoap->getOperationHistory($this->track_number)), true);
        $history = $result['OperationHistoryData']['historyRecord'] ?? null;
        $this->_history = $history;
//        Yii::info($this->_history[0], 'test');

        if (count($history) == 0) {
            Yii::warning('История трека пуста', 'warning');
            return false;
        }

//        Yii::info(json_decode(json_encode($history), true), 'test');

        if (!$this->start_waiting) {
            //Получаем дату получения статуса "Ожидает вручения"
            $this->start_waiting = $this->getStartWaitingDate($history);
        }

//        $firstHistory = $history[0];
        $firstHistory = $this->getFirstHistory();
        $lastHistory = $history[count($history) - 1];

        $this->setSenderZip();
//        $this->sender_zip = $firstHistory['AddressParameters']['OperationAddress']['Index'];

        Yii::info('Первое событие: ', 'test');
        Yii::info($firstHistory, 'test');
        Yii::info('Последнее событие: ', 'test');
        Yii::info($lastHistory, 'test');

        $this->last_status_updated_at = date('Y-m-d H:i:s', time());


        //Текущий статус
        $this->status = $lastHistory['OperationParameters']['OperAttr']['Name'] ?? null;
//        Yii::info('Новый статус: ' . $this->status, 'test');

        //Если нет статуса берем тип операции
        if (!$this->status) {
            $this->status = $lastHistory['OperationParameters']['OperType']['Name'] ?? null;
//            Yii::info('Берём тип операции: ' . $this->status, 'test');
        }

        //Получатель
        if (!$this->recipient) {
            $this->recipient = $result['OperationHistoryData']['historyRecord'][0]['UserParameters']['Rcpn'] ?? null;
        }
        if (!$this->name && $this->recipient) {
            $this->name = $this->recipient;
        }
        //Отправитель
        if (!$this->sender) {
            $this->sender = $firstHistory['UserParameters']['Sndr'] ?? null;
        }

        $this->send_date = date('Y-m-d',
                strtotime($firstHistory['OperationParameters']['OperDate'])) ?? null;

        $this->setTimeDays($lastHistory);

        $this->setDaysBack();

        //Текущий индекс
        $this->zip = $lastHistory['AddressParameters']['OperationAddress']['Index'] ?? null;
        //Индекс ПО назначения
        if (!$this->destination_zip) {
            //Если не сохранен индекс получателя
            $this->destination_zip = $firstHistory['AddressParameters']['DestinationAddress']['Index'] ?? null;
        }

        //Текущий адрес отправления
        $this->post_address = $lastHistory['AddressParameters']['OperationAddress']['Description'] ?? null;

        //Адерс ПО назначения
        if (!$this->destination_post_address) {
            //Если не сохранен адрес почты получателя
            $this->destination_post_address = $lastHistory['AddressParameters']['DestinationAddress']['Description'] ?? null;
        }
        if (isset($firstHistory['FinanceParameters']['Payment']) && $firstHistory['FinanceParameters']['Payment'] > 0) {
            $this->sum = round($firstHistory['FinanceParameters']['Payment'] / 100, 2);
        }

        //Добавляем информацию о посылке
        /** @var PackageInfo $pkg */
        $pkg = PackageInfo::find()->andWhere(['package_id' => $this->id])->one() ?? null;

        if (!$pkg) {
//            Yii::info('Доп информация не найдена, создаем', 'test');

            $pkg = new PackageInfo(['package_id' => $this->id]);
        }
//        else {
//            Yii::info('Доп. информация: ' . $pkg->id, 'test');
//        }

        $itemParam = $firstHistory['ItemParameters'] ?? null;
        $finance = $firstHistory['FinanceParameters'] ?? null;

//        Yii::info(json_decode(json_encode($pkg), true), 'test');

        if ($itemParam) {
//            Yii::info($itemParam, 'test');

            $complex_name = $itemParam['ComplexItemName'] ?? '';
            $pkg->type = $complex_name;

            if (isset($itemParam['Mass'])) {
                $pkg->weight = $itemParam['Mass'];
            } else {
                //Ищем по всей истории и устанавливаем массу
                $pkg->setMass($history);
            }
        }
        if ($finance) {
//            Yii::info($finance, 'test');

            if (isset($finance['Value']) && $finance['Value'] > 0) {
                $pkg->cost = round($finance['Value'] / 100, 2);
            }
            if (isset($finance['Payment']) && $finance['Payment'] > 0) {
                $pkg->pay_on_delivery = round($finance['Payment'] / 100, 2);
            }
        }
//        Yii::info($pkg->toArray(), 'test');
        if (!$pkg->save()) {
            Yii::error($pkg->errors, '_error');
        }

        //Добавляем ифну о переводе наложенного платежа
        (new OrderEvent(['package_id' => $this->id]))->setEvents();

        //Устанавливаем короткий статус по последнему статусу из нашего списка статусов (после определения индексов)
        $this->setLastShortStatus($history);

//        Yii::info($this->toArray(), 'test');

        if (!$this->save(false)) {
            Yii::error($this->errors, '_error');
            return false;
        }
        return true;
    }

    /**
     * Обновляет статусы 10 посылок которых статус <> Вручение адресату и с последнего обновления статус прошло больше 6 часов
     */
    public static function updateStatusByPeriod()
    {
        $timeRange = date('Y-m-d H:i:s', time() - (60 * 60 * 24 * 90));

        /** @var Package[] $packageStatuses */
        $packageStatuses = Package::find()
                ->andWhere([
                    'OR',
                    ['NOT IN', 'short_status', [2, 4]], //Не проверять статусы ВРУЧЕНО АДРЕСАТУ и ВРУЧЕНО ОТПРАВИТЕЛЮ
                    ['IS', 'status', null]
                ])
                ->andWhere([
                    'or',
                    ['<', 'last_status_updated_at', $timeRange],
                    ['last_status_updated_at' => null]
                ])
                ->limit(30)
                ->all() ?? '';

        foreach ($packageStatuses as $package) {
            Yii::info($package->toArray(), 'test');
            $package->updateStatus();
        }
    }

    public static function getList()
    {
        $result = self::find()
                ->select(['id', 'CONCAT(name, ": ", track_number, ", ", product_title) as name'])
                ->all() ?? null;
        return ArrayHelper::map($result, 'id', 'name');
    }

    /**
     * Меняет теги в сообщении на значения из базы
     * @param string $message_type Тип сообщения (call/sms)
     * @return string
     */
    public function getProcessedText($message_type)
    {
        Yii::info('Текст до обработки: ' . $this->text_to_process, 'test');

        $tags = [
            'track_number' => 'НомерРПО',
            'name' => 'ФИО',
            'product_title' => 'НаименованиеТовара',
            'sum' => 'Сумма',
            'zip' => 'Индекс',
            'post_address' => 'АдресОтделения',
            'days_back' => 'ДнейДоВозврата',
        ];

        foreach ($tags as $key => $tag) {
            $tag = '{' . $tag . '}';

//            Yii::info('Параметр на входе: ' . $tag, 'test');
//            Yii::info('Текст: ' . $this->text_to_process, 'test');
//            Yii::info('Результат поиска в тексте: ' . strpos($this->text_to_process, $tag), 'test');

            if (strpos($this->text_to_process, $tag) === false || strpos($this->text_to_process, $tag) === 'false') {
//                Yii::info('Параметр в тексте НЕ найден', 'test');
            } else {
//                Yii::info('Параметр в тексте найден', 'test');

                $value = $this->$key;

                //Дефисы в длинные номера проставляем только в звонках
                if ($message_type == self::MESSAGE_TYPE_CALL) {
//                    Yii::info('Значение до: ' . $value, 'test');
                    if ($key == 'track_number' || $key == 'zip') {
                        //Вставляем дефисы, иначе робот будет говорить "триллион..."
                        $parts = str_split($value, 3);
                        $value = implode('-', $parts);
                    }
                }

//                Yii::info('Параметр на выходе: ' . $value, 'test');

                $this->text_to_process = str_replace($tag, $value, $this->text_to_process);

//                Yii::info('Строка после обработки: ' . $this->text_to_process, 'test');
            }
        }
        $processing_result = $this->text_to_process;

        Yii::info('Текст после обработки: ' . $processing_result, 'test');

        return $processing_result;
    }

    /**
     * @return array Короткие статусы
     */
    public static function getShortStatusList()
    {
        return [
            0 => 'В ПУТИ',
            1 => 'ОЖИДАЕТ ВРУЧЕНИЯ',
            2 => 'ВРУЧЕНО АДРЕСАТУ',
            3 => 'ВОЗВРАЩЕНО',
            4 => 'ВРУЧЕНО ОТПРАВИТЕЛЮ',
            5 => 'ОТКАЗ АДРЕСАТА',
            6 => 'ВЫСЛАНО ОБРАТНО',
            7 => 'ОПЛАТА ПРИНЯТА',
            8 => 'ОПЛАТА ВЫПЛАЧЕНА',
        ];
    }

    public function getOperationsList()
    {
        return [
            1 => 'Прием',
            2 => 'Вручение',
            3 => 'Возврат',
            4 => 'Досылка почты',
            5 => 'Не вручение',
            6 => 'Хранение',
            7 => 'Временное хранение',
            8 => 'Обработка',
            12 => 'Неудачная попытка вручения',
            21 => 'Доставка',
            22 => 'Не доставка',
            42 => 'Вторая неудачная попытка вручения',
        ];
    }

    public function getOperationNameById($id)
    {
        return $this->getOperationsList()[$id] ?? '';
    }

    /**
     * Получает короткий статус из статса АПИ
     * @param string $status
     * @return int
     */
    public function getShortStatus($status = '')
    {
        Yii::info('Текущий статус: ' . $this->status, 'test');

        if (!$status) {
            $status = $this->status;
        }

        switch ($this->operation_id) {
            case 1: //Прием
                $short_status = 0; //В пути
                break;
            case 2: //Вручение
                //Проверяем оплату
                $payed = OrderEvent::find()
                    ->andWhere(['package_id' => $this->id])
                    ->andWhere(['type' => 3])->exists();

                if ($payed) {
                    //Если есть оплата
                    $short_status = 8; //Оплата выплачена
                } else {
                    //Если нет оплаты
                    //проверяем приём
                    $accept = OrderEvent::find()
                        ->andWhere(['package_id' => $this->id])
                        ->andWhere(['type' => 1])->exists();

                    if ($accept) {
                        //Если есть прием и нет оплаты
                        $short_status = 7; //Оплата принята
                    } else {
                        //Нет оплаты и нет приема (посылка без наложенного платежа)

                        $status = mb_strtolower($status);
                        if ($status == 'вручение адресату' || $status == 'вручение отправителю') {
                            if ($this->zip == $this->sender_zip) {
                                $short_status = 4; // Вручено отправителю
                            } else {
                                $short_status = 2; // Вручено адресату
                            }
                        } elseif (strpos($status, 'адресату') == false) {
                            //Если в статусе нет слова "адресату"
                            $short_status = 4; // Вручено отправителю
                        } else {
                            $short_status = 2; // Вручено адресату
                        }
                    }
                }
                break;
            case
            3: //Возврат
                if ($status == 'Отказ адресата') {
                    $short_status = 5; //Отказ адресата
                } else {
                    $short_status = 6; //Выслано обратно
                }
                break;
            case 8: //Обработка
                if ($status == 'Прибыло в место вручения') {
                    if ($this->zip == $this->sender_zip) {
                        //Если индекс операции совпадает с индексом отправителя
                        $short_status = 3; //Возвращено
                    } else {
                        $short_status = 1; //Ожидает вручения
                    }
                } else {
                    $short_status = 99;
                }
                break;
            default:
                $short_status = 99;
                break;
        }
        Yii::info('Операция: ' . $this->getOperationNameById($this->operation_id), 'test');

        return $short_status;
    }

    /**
     * Получает наименование короткого статуса по ID
     * @param int $id Идентификатор короткого статуса
     * @return mixed|null
     */
    public
    static function getShortStatusById(
        $id
    ) {
        if ($id == 99) {
            return null;
        }

//        Yii::info('Id короткого статуса: ' . $id);
        return self::getShortStatusList()[$id] ?? null;
    }

    public
    function getMessageHistory()
    {
        $messages = PackagePhonesSms::find()
                ->joinWith(['template tmp'])
                ->andWhere(['package_phones_sms.package_id' => $this->id])
                ->all() ?? null;

        Yii::info(json_decode(json_encode($messages), true), 'test');
        $str = '';
        /** @var PackagePhonesSms $message */
        foreach ($messages as $message) {
            $str .= $message->template->name . ' - ' . $message->datetime_send . ',';
        }
        return $str;
    }

    /**
     * Проверка условий перед созданием звонка
     * @return bool
     */
    public
    function preCheckCall()
    {
        //Проверяем кол-во повторных звонков адресату
        if ($this->num_re_call_finished > Settings::getValueByKey('number_re_call')) {
            $this->check_error = '<h4 class="danger">Превышено допустимое количество повторных звонков для ' . $this->phone . '</h4>';
            return false;
        }
        //Проверяем частоту звонков
        if ($this->date_re_call) {
            $new_recall_date = date('Y-m-d H:i:s',
                strtotime($this->date_re_call) + (Settings::getValueByKey('period_re_call') * 60));

            if (time() < strtotime($new_recall_date)) {
                $this->check_error = '<h4 class="danger">Превышение допустимой частоты звонков для ' . $this->phone . '</h4>';
                return false;
            }
        }
        return true;
    }

    /**
     * Получает дату с которой началось ожидание выдачи
     * @param array $history
     * @return false|string
     */
    private
    function getStartWaitingDate(
        $history
    ) {
//        $arr_history = json_decode(json_encode($history), true);
        //Перебираем с последнего элемента
        foreach (array_reverse($history) as $param) {
//            Yii::info($param['OperationParameters']['OperAttr'], 'test');
            $operation_name = $param['OperationParameters']['OperAttr']['Name'] ?? null;
//            Yii::info('Операция: ' . $operation_name, 'test');
            if ($operation_name == 'Прибыло в место вручения') {
//                Yii::info('Дата начала ожидания вручения: ' . date('Y-m-d H:i:s',
//                        strtotime($param['OperationParameters']['OperDate'])), 'test');
                return $this->start_waiting = date('Y-m-d H:i:s',
                    strtotime($param['OperationParameters']['OperDate']));
            }
        }
        return $this->start_waiting = null;
    }

    /**
     * Получает кол-вод ней в пути
     * @param \stdClass $last_history Последняя запись из истории посылки
     */
    private
    function setTimeDays(
        $last_history
    ) {
        if ($this->start_waiting) {
//            Yii::info('Начало ожидания: ' . $this->start_waiting, 'test');
//            Yii::info('Дата отправки: ' . $this->send_date, 'test');
            //Если посылка уже на почте
            $diff = strtotime($this->start_waiting) - strtotime($this->send_date);
        } else {
            //Вычитаем из последней даты операции даату отправки
            $diff = strtotime($last_history['OperationParameters']['OperDate']) - strtotime($this->send_date);
        }
        $this->time_days = (int)($diff / (60 * 60 * 24)); //Разница в днях
//        Yii::info('Дней в пути: ' . $this->time_days, 'test');
    }

    /**
     * Расчитывает кол-во дней до возврата (возврат через 30 дней)
     */
    private
    function setDaysBack()
    {
        if ($this->start_waiting) {
//            Yii::info('Начало ожидания: ' . $this->start_waiting, 'test');

            $diff = time() - strtotime($this->start_waiting);
            $days = (int)($diff / (60 * 60 * 24));
            $this->days_back = 30 - $days;
            if ($this->days_back < 0) {
                $this->days_back = 0;
            }
        } else {
            $this->days_back = null;
        }
//        Yii::info('Дней до возврата: ' . $this->days_back, 'test');

    }

    /**
     * Получает последний короткий статус
     * @param $history
     */
    private
    function setLastShortStatus(
        $history
    ) {
        //Проходимся по истории с конца
        foreach (array_reverse($history) as $param) {
            $cur_status = $param['OperationParameters']['OperAttr']['Name'] ?? null;
            $this->operation_id = $param['OperationParameters']['OperType']['Id'] ?? null;
            $short_status = $this->getShortStatus($cur_status);
            if ($cur_status && $short_status != 99) {
                $this->short_status = $short_status;
                break;
            }
        }
        Yii::info('РПО ' . $this->track_number . ' присвоен статус ' . self::getShortStatusById($this->short_status),
            'test');
    }

    /**
     * Обновление информации об отправлении (короткий статус не 'ВРУЧЕНО АДРЕСАТУ' и не 'ВРУЧЕНО ОТПРАВИТЕЛЮ')
     */
    public
    function forceUpdate()
    {
        if ($this->short_status <> 2 && $this->short_status <> 4 && $this->short_status <> 8) {
            $this->updateStatus();
        }
    }

    /**
     * Получаем индекс операции "Покинуло место приёма"
     * Код типа операции 8 (Обработка), код атрибута 1 (Покинуло место приёма)
     * @return null|string
     */
    private
    function setSenderZip()
    {
        if (!$this->_history) {
            return null;
        }

        foreach ($this->_history as $item) {
            if ($item['OperationParameters']['OperType']['Id'] == 8 && $item['OperationParameters']['OperAttr']['Id'] == 1) {
                return $this->sender_zip = $item['AddressParameters']['OperationAddress']['Index'];
            }
        }
        return null;
    }

    /**
     * Получает элемент истории со статусом "Прием" (первый элемент истории в котором содержиться вся инфа об отправлении)
     */
    private function getFirstHistory()
    {
        try {
            /** @var array $history_element */
            foreach ($this->_history as $history_element) {
                if ($history_element['OperationParameters']['OperType']['Id'] == 1) { //Прием
//                Yii::info($history_element, 'test');
                    return $history_element;
                    break;
                }
            }
        } catch (\Exception $e) {
            Yii::error($e->getMessage(), '_error');
        }

        return $this->_history[0]; //Если не найден "Прием", возвращаем первый элемент истории
    }

    /**
     * @param $ids
     * @return array
     * @throws \Throwable
     */
    public function removeSchemes($ids)
    {
        if (!$ids) {
            return ['success' => 0, 'error' => 'Ошибка при очистке схемы оповещения!'];
        }

        Yii::info('ID для удаления: ' . implode(', ', $ids), 'test');

        foreach ($ids as $id) {
            $package = Package::findOne($id) ?? null;

            if (!$package) {
                continue;
            }

            $package->deleteScheme();
        }

        return ['success' => 1];
    }

    /**
     * Обнуляет схему у отправления
     * @return bool
     * @throws \Throwable
     */
    private function deleteScheme()
    {
        //Очищаем настрйоки уведомлений для посылки
        /** @var Notice[] $ntps */
        $ntps = $this->noticesToPackage;

        foreach ($ntps as $ntp) {
            Yii::info($ntp->toArray(), 'test');

            try {
                $ntp->delete();
            } catch (\Exception $e) {
                Yii::error($e->getMessage(), '_error');
            }
        }

        //Обнуляем схему
        $this->scheme_id = null;
        if (!$this->save()) {
            Yii::error($this->errors, '_error');
            return false;
        }

        return true;
    }

    /**
     * Проверяет схемы. Полностью отработавшие схемы удаляет из посылки
     * Если выполнены все задания схемы - обнуляет поле схемы и удаляет все задания оповщения для посылки
     */
    public function cleanSchemes()
    {

        $query = NoticeToPackage::find()
            ->joinWith(['package'])
            ->andWhere(['NOT', ['package.scheme_id' => null]]);
//            ->groupBy(['notice_to_package.status']);

        /** @var NoticeToPackage $ntp */
        foreach ($query->each() as $ntp){

            $isFinished = $ntp->notice->scheme->isFinished($ntp->package_id);

            if ($isFinished){

                $ntp->deleteAllForPackage($ntp->package_id);

                //Все оповещения отработали
                //Обнуляем схему для посылки
                $package = $ntp->package;
                $package->scheme_id = null;

                if (!$package->save()){
                    Yii::error($package->errors, '_error');
                }
            }
        }
        return true;
    }

    public function removeComments($packages_id)
    {
        foreach ($packages_id as $package_id){
            $package = Package::findOne($package_id) ?? null;
            if (!$package) {
                continue;
            }
            $package->comment = null;
            $package->save(false);
        }
        return ['success' => 1];
    }

    public function checkNotifyRelations()
    {
        return true;
    }
}
