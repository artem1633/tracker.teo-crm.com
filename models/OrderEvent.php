<?php

namespace app\models;

use app\services\RusMailSoap;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "order_event".
 *
 * @property int $id
 * @property int $package_id
 * @property string $number Номер
 * @property string $date Дата события
 * @property int $type Тип
 * @property string $name Наименование
 * @property string $index_to Индекс получателя
 * @property string $index Индекс места события
 * @property int $sum Сумма наложенного платежа, коп.
 *
 * @property Package $package
 */
class OrderEvent extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['package_id', 'type', 'sum'], 'integer'],
            [['date'], 'safe'],
            [['number', 'name', 'index_to', 'index'], 'string', 'max' => 255],
            [
                ['package_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Package::className(),
                'targetAttribute' => ['package_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'package_id' => 'Package ID',
            'number' => 'Номер',
            'date' => 'Дата события',
            'type' => 'Тип',
            'name' => 'Операция',
            'index_to' => 'Индекс адресата',
            'index' => 'Индекс операции',
            'sum' => 'Сумма наложенного платежа, коп.',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackage()
    {
        return $this->hasOne(Package::className(), ['id' => 'package_id']);
    }

    /**
     * Проверяет и устанавливает события почтового отправления для посылки
     */
    public function setEvents(): bool
    {
        if (!$this->package_id) {
            return false;
        }

        $rpo = $this->package->track_number ?? null;

        $login = Yii::$app->session->get('pr_login');
        $pass = Yii::$app->session->get('pr_pass');

        if (!$login || !$pass) {
            $login = Settings::findByKey('mail_login')->value;
            $pass = Settings::findByKey('mail_password')->value;
        }

        $client = new RusMailSoap($login, $pass);
//        $rpo = '80088639949347';
        $result = $client->getPostalOrderEvents($rpo);
//        Yii::info($result, 'test');

        foreach ($result as $event) {
//            Yii::info($event['NUMBER'], 'test');
//            Yii::info($event['EVENTDATETIME'], 'test');

            $exist_model = self::find()
                ->andWhere(['package_id' => $this->package_id])
                ->andWhere(['index' => $event['INDEXEVENT']])
                ->andWhere(['number' => $event['NUMBER']])
                ->andWhere(['type' => $event['EVENTTYPE']])
                ->exists();

            if (!$exist_model){
                Yii::info('Операция не найдена, создаем новую.', 'test');
                $model = new OrderEvent(
                    [
                        'package_id' => $this->package_id,
                        'number' => $event['NUMBER'],
                        'date' => date('Y-m-d H:i:s', strtotime($event['EVENTDATETIME'])),
                        'type' => $event['EVENTTYPE'],
                        'name' => $event['EVENTNAME'],
                        'index_to' => $event['INDEXTO'],
                        'index' => $event['INDEXEVENT'],
                        'sum' => $event['SUMPAYMENTFORWARD'],
                    ]
                );
//                Yii::info($model->toArray(), 'test');
                if (!$model->save()){
                    Yii::error($model->errors, '_error');
                }
            } else {
                Yii::info('Операция уже в базе, пропускаем', 'test');
            }
        }
        return true;
    }

    public function getTypeList()
    {
        return [

        ];
    }
}
