<?php

namespace app\models;

use app\models\query\NoticeToPackageQuery;
use app\modules\api\models\Message;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * This is the model class for table "notice_to_package".
 *
 * @property int $id
 * @property int $notice_id
 * @property int $package_id
 * @property string $exec_date Дата выполнения
 * @property string $last_exec_date Дата последнего выполнения
 * @property string $status Статус опопвещения
 * @property int $exec_num Кол-во выполненных оповещений
 *
 * @property Notice $notice
 * @property Package $package
 */
class NoticeToPackage extends ActiveRecord
{
    const STATUS_TO_WORK = 'work';
    const STATUS_FINISHED = 'finished';

    /** @var string */
    public $preparedText;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notice_to_package';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notice_id', 'package_id', 'exec_num'], 'integer'],
            [['exec_date', 'last_exec_date'], 'safe'],
            [['status'], 'string', 'max' => 255],
            [
                ['notice_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Notice::className(),
                'targetAttribute' => ['notice_id' => 'id']
            ],
            [
                ['package_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Package::className(),
                'targetAttribute' => ['package_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'notice_id' => 'Notice ID',
            'package_id' => 'Package ID',
            'exec_date' => 'Дата выполнения',
            'last_exec_date' => 'Дата последнего выполнения',
            'status' => 'Статус опопвещения',
            'exec_num' => 'Кол-во выполненных оповещений',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotice()
    {
        return $this->hasOne(Notice::className(), ['id' => 'notice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackage()
    {
        return $this->hasOne(Package::className(), ['id' => 'package_id']);
    }

    /**
     * @inheritdoc
     * @return NoticeToPackageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NoticeToPackageQuery(get_called_class());
    }

    /**
     * Рассылает опопвещения согласно схеме оповещений
     */
    public function notifyByScheme()
    {
        //Получаем настройки всех активных уведомлений
        $query = NoticeToPackage::find()
            ->andWhere(['status' => NoticeToPackage::STATUS_TO_WORK])
            ->andWhere([
                'OR',
                ['<', 'exec_date', date('Y-m-d H:i:s', time())],
                ['IS', 'exec_date', null]
            ]);

        Log::log('Начало проверки всех активных уведомлений.');

        /** @var NoticeToPackage $ntp */
        foreach ($query->each() as $ntp) {

            Yii::info('Оповещение: ' . $ntp->notice->name ?? 'Неизвестный', 'test');

            $package = $ntp->package; //Посылка

            $notice = $ntp->notice;

            Log::log('РПО: ' . $package->track_number);
            Log::log('Оповещение. Наименование: ' . $notice->name ?? 'Неизвестный');
            Log::log('Оповещение. Статус срабатывания: ' . $notice->short_status ?? 'Неизвестный');
            Log::log('Оповещение. Интервал: ' . $notice->interval ?? 'Не определен');
            Log::log('Оповещение. Кол-во повторений: ' . $notice->repeat_num ?? 'Не определено');
            Log::log('Оповещение. Дней до возврата: ' . $notice->day_to_return ?? 'Не определено');
            Log::log('Оповещение. Шаблон: ' . $notice->template->name ?? 'Не определен');
            Log::log('Оповещение. Тип шаблона: ' . $notice->template->type ?? 'Не определен');
            Log::log('Статус оповещения: ' . $ntp->status);
            Log::log('Кол-во выполненных оповещений: ' . $ntp->exec_num ?? 0);
            Log::log('Последняя дата выполнения: ' . $ntp->last_exec_date ?? 'Не указана');
            Log::log('Следующая дата выполнения: ' . $ntp->exec_date ?? 'Не указана');

            Yii::info('РПО: ' . $package->track_number, 'test');

            //Получаем настройку "Дней до возврата"
            $day_to_return = $notice->day_to_return ?? 0;

            Yii::info('Дней до возврата: ' . $day_to_return, 'test');

            if ($ntp->exec_date) {
                Yii::info('Дата напоминания: ' . $ntp->exec_date, 'test');

                //Дата выполения указана
                if ($package->short_status == $ntp->notice->short_status && $ntp->exec_date < date('Y-m-d H:i:s', time())) {
                    Yii::info('Отправляем сообщение.', 'test');
                    Log::log('Дата оповещения меньше текущей');
                    Log::log('Короткий статус посылки совпадает со статусом срабатывания оповещения');

                    //Короткий статус отправления совпадает со статусом из настройки уведомления и дата выполнения меньше текущей
                    $template = $ntp->notice->template; //Шаблон сообщения

                    //Отправляем сообщение
                    $result = $this->sendNotify($template->type, $ntp);

//                    if ($result['success'] == 0){
//                        //Ошибка отправки
//                        Yii::warning($result['data'], 'warning');
//                        //Увеличиваем дату выполнения на час
//                        $ntp->exec_date = date('Y-m-d H:i:s', time() + 60 * 60);
//                        continue;
//                    }
                    Yii::info($result, 'test');

                    if ($day_to_return > 0) {
                        Log::log('Настройка дней до возврата определена. Изменение статуса оповещения на "finished"');
                        //Выставляем статус завершено для данного уведомления
                        $ntp->finishNotice();
                        continue;
                    } else {
                        Log::log('Настройка дней до возврата НЕ определена.');

                        //Нет настройки "Дней до возврата"
                        $interval = $ntp->notice->interval * 24 * 60 * 60 ?? 0; //Интервал измеряется в днях

                        Yii::info('Интервал напоминаний: ' . $interval . ' сек.', 'test');

                        if ($interval) {
                            //отсчитываем интервал от текущей даты (т.к. оповещение только-что отправлено)
                            $ntp->exec_date = date('Y-m-d 10:00:00', time() + $interval);

                            Log::log('Дата следующего напоминания: ' . $ntp->exec_date);
                            Yii::info('Следующая дата напоминания: ' . $ntp->exec_date, 'test');
                        } else {
                            Log::log('Интервал не определен. Изменение статуса оповещения на "finished"');
                            $ntp->finishNotice();
                            continue;
                        }

                        $repeat_num = $ntp->notice->repeat_num ?? 0;
                        Yii::info('Кол-во повторений: ' . $repeat_num , 'test');

                        if ($repeat_num) {
                            $ntp->exec_num += 1;
                            if ($ntp->exec_num > $repeat_num) {
                                Log::log('Кол-во повторных оповещений превышает допустимый предел (' . $repeat_num . ' раз). Изменение статуса оповещения на "finished"');

                                Yii::info('Кол-во совершенных оповещений исчерпано', 'test');

                                //Кол-во проведенных оповещений больше чем указано в настройке уведомления
                                //Выставляем статус завершено для данного уведомления
                                $ntp->exec_date = null;
                                $ntp->status = self::STATUS_FINISHED;
                            } else {
                                //Повторения не исчерпаны.
                                //Переписываем следующую дату выполнения оповещения. Дата выполнения оповещения = текущая дата (т.к. напоминание только что отправлено) + интервал
                                $ntp->exec_date = date('Y-m-d 10:00:00', time() + $interval);

                                Log::log('Следующее повторное оповещение: ' . $ntp->exec_date);

                                Yii::info('Следующая дата оповешения: ' . $ntp->exec_date, 'test');
                            }
                        } else {
                            //Напоминание отправлено, повторения не указаны (ноль), завершаем опопвещение
                            $ntp->finishNotice();
                        }
                    }
                } else {
                    Yii::info('Не соблюдены все условия, пропускаем', 'test');
                    Log::log('Дата оповещения больше текущей или статус посылки не совпадает со статусом оповещения');
                    continue;
                }
            } else {
                Log::log('Дата следующего оповещения НЕ найдена');

                Yii::info('Дата оповещения не найдена', 'test');

                if ($ntp->last_exec_date) {
                    Yii::info('Дата последнего исполненного оповещения найдена. Статус оповещения "завершено"', 'test');
                    Log::log('Дата последнего исполненного оповещения найдена. Присваиваем оповещению статус "finished"');

                    //Дата последнего выполнения указана
                    $ntp->exec_date = null;
                    $ntp->status = self::STATUS_FINISHED;
                } else {
                    Yii::info('Дата последнего оповещения НЕ найдена.', 'test');
                    Log::log('Дата последнего оповещения НЕ найдена');

                    //Дата последнего выполнения не указана
                    if ($package->short_status == $ntp->notice->short_status) {
                        Yii::info('Статус в настройке: ' . $ntp->notice->short_status, 'test');
                        Yii::info('Статус посылки: ' . $package->short_status, 'test');

                        Log::log('Статус посылки и статус оповещения совпадают');

                        if ($day_to_return > 0) {

                            Yii::info('Настройка дней до возврата: ' . $day_to_return, 'test');
                            Log::log('Настройка дней до возврата: ' . $day_to_return);

                            //Устанавливаем дату выполнения (оповещение за указанное кол-во дней до возврата)
                            $ntp->exec_date = date('Y-m-d 10:00:00',
                                strtotime($package->start_waiting) + ((30 - $day_to_return) * 24 * 60 * 60));
                            Log::log('Дата следующего напоминания: ' . $ntp->exec_date);
                            Yii::info('Дата следующего напоминания: ' . $ntp->exec_date, 'test');

                        } else {
                            Yii::info('Настройка дней до возврата не найдена', 'test');
                            Log::log('Настройка дней до возврата не найдена');

                            //Нет настройки "Дней до возврата"
                            $interval = $ntp->notice->interval * 24 * 60 * 60 ?? 0; //Интервал измеряется в днях

                            Yii::info('Настройка интервал: ' . $interval, 'test');

                            if ($interval) {
                                $ntp->exec_date = date('Y-m-d 10:00:00',
                                    strtotime($package->start_waiting) + $interval);
                                Log::log('Дата следующего напоминания: ' . $ntp->exec_date);
                                Yii::info('Дата следующего напоминания: ' . $ntp->exec_date, 'test');
                            } else {
                                $ntp->exec_date = date('Y-m-d H:i:s', time());
                                Log::log('Интервал не определен. Дата следующего напоминания (текущее время): ' . $ntp->exec_date);
                                Yii::info('Интервал не определен. Дата оповещения = ' . $ntp->exec_date, 'test');
                            }
                        }
                    } else {
                        Log::log('Короткие статусы в оповещении и посылке не совпадают');

                        Yii::info('Корткий статус в настройке и впосылке не совпадают' . $ntp->exec_date, 'test');
                        //Короткий статус посылки не совпадаетс с коротким статусом в настройке оповещения
                        continue;
                    }
                }
            }

            //Устанавливаем дату последнего выполнения
            $ntp->last_exec_date = date('Y-m-d H:i:s', time());

            Log::log('Дата последнего выполнения установлена: ' . $ntp->last_exec_date);

            if (!$ntp->save()) {
                Log::log('Ошибка записи в NoticeToPackage. ' . json_encode($ntp->errors));
                Yii::error($ntp->errors, '_error');
            }
            Log::log('Запись в NoticeToPackage сохранена. ID записи: ' . $ntp->id);
        }

        Log::log('Завершение проверки всех активных уведомлений.');

        return true;
    }

    /**
     * Добавление звонка
     * @return array|bool
     */
    public function makeCall()
    {
        Log::log('Подготовка к совершению звонка');

        $package = Package::findOne($this->package_id);
        $template = SmsTemplate::findOne($this->notice->template_id);
        $data = [];

        if ($package == null || $template == null) {
            Log::log('Не найдена посылка или шаблон сообщения. Пропускаем');
            return ['success' => 0, $data['errors'] => 'Не найдена посылка или шаблон сообщения'];
        }

        $callText = $template->content;

        $package->text_to_process = $callText;

        //Обрабатываем тэги в тексте
        $callText = $package->getProcessedText(Package::MESSAGE_TYPE_CALL);

        $response = Yii::$app->voiceService->send($package->phone, $callText);

        Log::log('Результат звонка: ' . (string)$response);

        $response = Json::decode($response);

        Yii::info($response, 'test');

        if ($response['status'] == 'success') {
            $model = new PackagePhonesSms([
                'call_id' => $response['data'][0]['id'],
                'package_id' => $package->id,
                'text' => $callText,
                'phone' => $package->phone,
                'created_at' => date('Y-m-d H:i:s', $response['data'][0]['createdAt']),
                'template_id' => $template->id,
            ]);
            if (!$model->save(false)) {
                Yii::error($model->errors, '_error');
                $success = 0;
                $data['save_errors'] = $model->errors;

                Log::log('Ошибка сохранения звонка: ' . (string)$response);
            } else {
                Log::log('Звонок сохранен успешно. ID записи: ' . $model->id);

                $success = 1;
            }
        } else {
            Log::log('Не сохранено. Статус в ответе: ' . $response['status'] ?? null);

            Yii::error($response, '_error');
            $success = 0;
            $data = $response['data']['message'];
        }

        return ['success' => $success, 'data' => $data];
    }

    /**
     * Формирует и отправляет СМС
     * Возвращает массив
     * [
     *  'success' => <0/1>
     *  'data' => <null или описание ошибки>
     * ]
     * @return array
     */
    public function makeSms()
    {
        Log::log('Подготовка к отправке СМС');

        $package = Package::findOne($this->package_id) ?? null;
        $template = SmsTemplate::findOne($this->notice->template_id) ?? null;
        $data = [];

        if ($package == null || $template == null) {
            return ['success' => 0, 'data' => ['error_send_sms' => 'package or template not found']];
        }
//        Yii::info($package->toArray(), 'test');

        $smsText = $template->content;

        //Обрабатываем тэги в тексте
        $package->text_to_process = $smsText;
        $smsText = $package->getProcessedText(Package::MESSAGE_TYPE_SMS);

        $response = Yii::$app->smsService->send($package->phone, $smsText);

        Log::log('Результат отправки СМС: ' . (string)$response);

        $response = Json::decode($response);

        Yii::info($response, 'test');

        if ($response['result'] == 'reject') {
            Yii::error($response['reason'], '_error');
            $success = 0;
            $data['error_send_sms'] = $response['reason'];
        } else {
            $model = new PackagePhonesSms([
                'sms_id' => $response['id'],
                'package_id' => $package->id,
                'text' => $smsText,
                'phone' => $package->phone,
                'template_id' => $template->id,
            ]);

            if (!$model->save(false)) {
                Yii::error($model->errors, '_error');
                $success = 0;
                $data['save_errors'] = $model->errors;
                Log::log('Ошибка сохранения информации об отправленном СМС. ' . json_encode($model->errors));
            } else {
                Log::log('Информация об отправленном СМС сохранена. ID записи: ' . $model->id);
                $success = 1;
            }
        }

        return ['success' => $success, 'data' => $data];
    }

    /**
     * Отбор по типу сообщения и отправка сообщения
     * @param string $type Тип сообщения (sms/call)
     * @param NoticeToPackage $ntp
     * @return array
     */
    public function sendNotify($type, NoticeToPackage $ntp)
    {
        if (!$type) {
            return ['success' => 0, 'data' => ['send_notify' => 'Type not found']];
        }

        if ($type == Message::TYPE_SMS) {
            return $ntp->makeSms();
        } elseif ($type == Message::TYPE_CALL) {
            return $ntp->makeCall();
        }

        return ['success' => 0];
    }

    /**
     * Удаляет все оповещения для посылки
     * @param $package_id
     * @return int
     */
    public function deleteAllForPackage($package_id)
    {
        return self::deleteAll(['package_id' => $package_id]);

    }

    /**
     * Очищает дату выполнения и выставляет статус завершения
     * @return bool;
     */
    private function finishNotice()
    {
        $this->exec_date = null;
        $this->status = self::STATUS_FINISHED;

        if (!$this->save(false)) {
            Yii::error($this->errors, '_error');
            return false;
        }

        return true;
    }

}
