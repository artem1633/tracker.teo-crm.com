<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "scheme".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $type Тип [sms|call]
 * @property string $description Описание
 * @property string $templates Шаблоны оповещений (в форме конфигурации)
 *
 * @property Notice[] $notices
 */
class Scheme extends ActiveRecord
{

    const TYPE_SMS = 'sms';
    const TYPE_CALL = 'call';

    /** @var array */
    public $templates;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scheme';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['name', 'type'], 'string', 'max' => 255],
            [['name'], 'required'],
            ['templates', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'type' => 'Тип схемы',
            'description' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotices()
    {
        return $this->hasMany(Notice::className(), ['scheme_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackages()
    {
        return $this->hasMany(Package::className(), ['scheme_id', 'id']);
    }

    /**
     * Список схем
     * @return array
     */
    public function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

    public function getTypeList()
    {
        return [
            self::TYPE_SMS => 'СМС сообщения',
            self::TYPE_CALL => 'Звонки на телефон',
        ];
    }

    public function getTypeNameByKey($key)
    {
        return $this->getTypeList()[$key];
    }

    public function isFinished($package_id)
    {
        //Кол-во оповещений в схеме
        $notice_count = Notice::find()
            ->andWhere(['scheme_id' => $this->id])
            ->count();

        //Завершенные оповещения для посылки
        $finished_count = NoticeToPackage::find()
            ->andWhere(['package_id' => $package_id])
            ->andWhere(['status' => NoticeToPackage::STATUS_FINISHED])
            ->count();

        if ($notice_count == $finished_count){
            //Если кол-во оповещений в схеме равно кол-ву завершенных опопвещений
            return true;
        } else {
            return false;
        }
    }

}
