<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PackageSearch represents the model behind the search form about `app\models\Package`.
 */
class PackageSearch extends Package
{


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'time_days', 'days_back', 'payed', 'short_status', 'scheme_id'], 'integer'],
            [
                [
                    'track_number',
                    'send_date',
                    'phone',
                    'name',
                    'product_title',
                    'status',
                    'last_status',
                    'comment',
                    'created_at',
                    'start',
                    'end',
                    'init_day',
                    'final_day',
                    'show_comment',
                    'show_empty_comment',
                ],
                'safe'
            ],
            [['sum'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Package::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'send_date' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
//            'send_date' => $this->send_date,
            'sum' => $this->sum,
            'time_days' => $this->time_days,
            'days_back' => $this->days_back,
            'payed' => $this->payed,
            'created_at' => $this->created_at,
            'short_status' => $this->short_status,
            'scheme_id' => $this->scheme_id,
        ]);

        $query->andFilterWhere(['like', 'track_number', $this->track_number])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'product_title', $this->product_title])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'last_status', $this->last_status])
            ->andFilterWhere(['LIKE', 'send_date', $this->send_date])
            ->andFilterWhere(['LIKE', 'comment', $this->comment]);

        if ($this->show_comment) {
            Yii::info('Показывать только с комментариями', 'test');
            $query->andWhere(['<>', 'comment', '']);
        } elseif ($this->show_empty_comment) {
            Yii::info('Показывать только без комментариев', 'test');
            $query->andWhere(['OR', ['=', 'comment', ''], ['IS', 'comment', null]]);
        }

        if ($this->start) {
            $this->start = date('Y-m-d', strtotime($this->start));
            Yii::info($this->start, 'test');
            if (!$this->end) {
                $this->end = date('Y-m-d', time());
            } else {
                $this->end = date('Y-m-d', strtotime($this->end));
            }
            Yii::info($this->end, 'test');

            $query->andWhere(['BETWEEN', 'send_date', $this->start, $this->end]);
        }

        if ($this->init_day && $this->final_day) {
            $query
                ->andWhere(['NOT IN', 'short_status', ['']])
                ->andWhere(['NOT', ['days_back' => null]])
                ->andWhere(['BETWEEN', 'days_back', $this->init_day, $this->final_day]);
        }


//        $query->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
