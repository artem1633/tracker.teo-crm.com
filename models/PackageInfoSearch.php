<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PackageInfoSearch represents the model behind the search form about `app\models\PackageInfo`.
 */
class PackageInfoSearch extends PackageInfo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'package_id'], 'integer'],
            [['type'], 'safe'],
            [['weight', 'cost', 'pay_on_delivery'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PackageInfo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'package_id' => $this->package_id,
            'weight' => $this->weight,
            'cost' => $this->cost,
            'pay_on_delivery' => $this->pay_on_delivery,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
