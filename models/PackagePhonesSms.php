<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * This is the model class for table "package_phones_sms".
 *
 * @property int $id
 * @property string $sms_id Идентификатор сообщения
 * @property string $call_id Идентификатор сообщения
 * @property int $package_id Посылка
 * @property string $phone Телефон
 * @property string $status Статус
 * @property string $text Текст сообщения
 * @property string $datetime_send Дата и время отправки
 * @property string $datetime_deliver Дата и время доставки
 * @property string $created_at
 * @property string $type Тип сообщения (звонок, смс)
 * @property string $message_id Идентификатор сообщения
 * @property int $template_id Идентификатор шаблона сообщения
 * @property double $cost Цена звокна/смс
 * @property double $outer_status Статус сообщения, присланный сервисом отправки сообщений
 * @property int $notice_id Идентификатор оповещения (то, которое на основе схемы оповещения)
 *
 * @property Package $package
 * @property SmsTemplate $template
 */
class PackagePhonesSms extends ActiveRecord
{
    public $type;
    public $message_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'package_phones_sms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['package_id', 'template_id', 'notice_id'], 'integer'],
            [['text', 'type', 'message_id', 'outer_status'], 'string'],
            [['datetime_send', 'datetime_deliver', 'created_at'], 'safe'],
            [['sms_id', 'call_id', 'phone', 'status'], 'string', 'max' => 255],
            [
                ['package_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Package::className(),
                'targetAttribute' => ['package_id' => 'id']
            ],
            [['cost'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sms_id' => 'Идентификатор СМС сообщения',
            'call_id' => 'Идентификатор звонка',
            'package_id' => 'Посылка',
            'phone' => 'Телефон',
            'status' => 'Статус',
            'text' => 'Текст сообщения',
            'datetime_send' => 'Дата и время отправки',
            'datetime_deliver' => 'Дата и время доставки',
            'created_at' => 'Created At',
            'type' => 'Тип',
            'message_id' => 'Идентификатор сообщения',
            'template_id' => 'Идентификатор шаблона сообщения',
            'cost' => 'Цена (руб.)',
            'outer_status' => 'Статус от сервиса',
            'notice_id' => 'Опопвещение (из схемы оповещения)',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->phone) {
            $this->phone = str_replace('-', '', $this->phone);
        }
        if ($insert) {
            $this->created_at = date('Y-m-d H:i:s', time());
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackage()
    {
        return $this->hasOne(Package::className(), ['id' => 'package_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(SmsTemplate::className(), ['id' => 'template_id']);
    }

    /**
     * Проверка статуса звонка
     * @param string $id Идентификатор звонка
     * @return array
     */
    public function checkCall($id)
    {
        Yii::info('Проверка статутса звонка: ID ' . $id, 'test');

        $package = Package::findOne($this->package_id);
        $success = 0;
        $data = 'Неизвестная ошибка';

        $result = Yii::$app->voiceService->checkStatus($id);

//        Yii::info($result, 'test');

        if ($result) {
            try {
                $result = Json::decode($result);
            } catch (\Exception $e) {
                Yii::error($e->getMessage(), 'error');
                return ['success' => 0, 'data' => $e->getMessage()];
            }
        }

        //Проверка прошла без ошибок со стороны сервиса звонков
        Yii::info($result, 'test');

        if ($result['status'] == 'success') {
            $data = $result['data'][0] ?? null;
//            Yii::info($data, 'test');

            if ($data) {
                $status_call = $data['calls'][0]['status'] ?? null;
                /** @var PackagePhonesSms $model */
                $model = PackagePhonesSms::find()->andWhere(['call_id' => $data['id']])->one() ?? null;

                if ($model) {
                    $model->outer_status = $status_call;
                    $model->cost = $data['calls'][0]['cost'] ?? null;

                    //Проверяем на минимальную стоимость звонка. Проверка добавлена, т.к. в некоторых случаях сервис врёт.
                    Yii::info('Цена звонка: ' . $model->cost, 'test');
                    Yii::info('Минимальное ограничение на цену: ' . Settings::getValueByKey('min_cost_call'), 'test');

                    if (($model->cost < Settings::getValueByKey('min_cost_call') && $model->outer_status == 'finished')) {
                        Yii::info('Статус звонка "finished", но цена звонка не достигла минимального значения', 'test');
                        $model->status = 'canceled';

                        //После звонка записываем кол-во совершенных звонков адресату
                        $package->num_re_call_finished += 1;
                        $package->date_re_call = date('Y-m-d H:i:s', time());

                        if (!$package->save()) {
                            Yii::error($package->errors, '_error');
                        }
                    } else {
                        $model->status = $status_call;
                    }

                    $model->datetime_send = date('Y-m-d H:i:s', $data['calls'][0]['startedAt']);
                    $answered_at = $data['calls'][0]['answeredAt'] ?? null;

                    Yii::info('answered_at: ' . $answered_at, 'test');

                    //Если нет даты ответа, выставляем дату ответа равной дате совершения звонка
                    try {
                        $model->datetime_deliver = date('Y-m-d H:i:s', $answered_at);
                    } catch (\Exception $e) {
                        $model->datetime_deliver = date('Y-m-d H:i:s', $model->datetime_send);
                    }

                    Yii::info($model->toArray(), 'test');
                    if (!$model->save()) {
                        Yii::error($model->errors, '_error');
                        $success = 0;
                        $data = $model->errors;
                    } else {
                        $success = 1;
                        $data = '';
                    }
                } else {
                    $data = 'Звонок не найден в базе';
                }
            } else {
                $data = 'Информация не найдена';
            }
        }
        return ['success' => $success, 'data' => $data];
    }

    /**
     * Проверка статуса СМС сообщения
     * @param int $id Идентификатор сообщения
     * @return array
     */
    public function checkSms($id)
    {
        $success = 0;
        $data = 'Неизвестная ошибка';
        $result = Json::decode(Yii::$app->smsService->checkStatus($id));

        Yii::info($result, 'test');

        Log::log('СМС результат запроса статуста: ' . json_encode($result), Log::LOG_TYPE_CHECK_SMS);

        $status_id = $result['result'] ?? null;

        if ($status_id != 'reject') {
            /** @var PackagePhonesSms $model */
            $model = PackagePhonesSms::find()->andWhere(['sms_id' => $id])->one() ?? null;
            if (!$model) {
                $data = 'СМС сообщение не найдено в базе';
            } else {
                $model->status = $status_id;
                //Время приходит строкой в формате Y-m-d H:i:s и Московское. для записи в базу вычиатем три часа (в базе время UTC)
                $model->datetime_send = date('Y-m-d H:i:s', strtotime($result['date_send']) - (60 * 60 * 3)) ?? null;

                if ($status_id == 'delivery success') {
                    //Пишем в базу дату доставки
                    $model->datetime_deliver = date('Y-m-d H:i:s',
                            strtotime($result['date_deliver']) - (60 * 60 * 3)) ?? null;
                }
                if (!$model->save()) {
                    Yii::error($model->errors, 'error');
                    $data = $model->errors;
                } else {
                    $success = 1;
                }
            }
        } else {
            $data = $result['reason'];
        }

        return ['success' => $success, 'data' => $data];
    }

    /**
     * Возвращает статусы звонков
     * https://lk.zvonobot.ru/panel/apiInfo
     */
    public function getListCallStatuses()
    {
        return [
            'processed' => 'В процессе обработки',
            'created' => 'Создан но не начат',
            'finished' => 'Закончен и разговор состоялся',
            'canceled' => 'Закончен и разговор не состоялся',
            'answered' => 'В процессе разговора',
            'busy' => 'Закончен и абонент занят',
            'started' => 'Начат',
        ];
    }

    /**
     * Получает описание статуса звонка
     * @param string $id Идентификатор звонка
     * @return string
     */
    public function getCallStatusName($id)
    {
        return $this->getListCallStatuses()[$id];
    }

    /**
     * Возвращает статусы СМС сообщений
     * https://smsaero.ru/api/v1/
     */
    public function getListSmsStatuses()
    {
        return [
            'delivery success' => 'Сообщение доставлено',
            'delivery failure' => 'Ошибка доставки SMS (Возможно абонент вне действия сети)',
            'smsc submit' => 'Сообщение передано оператору',
            'smsc reject' => 'Сообщение отклонено',
            'queue' => 'Ожидает отправки',
            'wait status' => 'Ожидание статуса',
            'incorrect id. reject' => 'Неверный идентификатор сообщения',
            'empty field. reject' => 'Не все обязательные поля заполнены',
            'incorrect user or password. reject' => 'Ошибка авторизации'
        ];
    }

    /**
     * Получает описание статуса СМС сообщения
     * @param int $id Идентификатор СМС сообщения
     * @return string
     */
    public function getSmsStatusName($id)
    {
        return $this->getListSmsStatuses()[$id];
    }
}
