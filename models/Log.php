<?php

namespace app\models;

use app\models\query\LogQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "notify_log".
 *
 * @property int $id
 * @property string $created_at Дата события
 * @property int $type Тип лога
 * @property string $log Содержание
 */
class Log extends ActiveRecord
{

    const LOG_TYPE_NOTIFY = 1;
    const LOG_TYPE_CHECK_SMS = 2;
    const LOG_TYPE_CHECK_CALL = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['type'], 'integer'],
            [['log'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Дата события',
            'type' => 'Тип лога',
            'log' => 'Содержание',
        ];
    }

    /**
     * @inheritdoc
     * @return LogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LogQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        if ($insert){
            $this->created_at = date('Y-m-d H:i:s', time());
        }
        return parent::beforeSave($insert);
    }

    public function getList()
    {
        return [
            self::LOG_TYPE_NOTIFY => 'Уведомления',
            self::LOG_TYPE_CHECK_SMS => 'СМС',
            self::LOG_TYPE_CHECK_CALL => 'Звонок'
        ];
    }

    /**
     * Записывает лог
     * @param string $event Текст, описывающие событие
     * @param int $type Тип лога
     *
     * @return bool
     */
    public static function log($event, $type = self::LOG_TYPE_NOTIFY)
    {
        if (YII_ENV_DEV) {
            $log = new Log([
                'created_at' => date('Y-m-d H:i:s', time()),
                'type' => $type,
                'log' => $event,
            ]);

            if (!$log->save()) {
                Yii::error($log->errors, '_error');
                return false;
            }
        }

        return true;
    }
}
