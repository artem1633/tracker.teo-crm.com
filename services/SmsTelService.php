<?php

namespace app\services;

use app\models\Settings;
use yii\base\Component;

/**
 * Class SmsTelService
 * @package app\services
 */
class SmsTelService extends Component
{
    public $login;
    public $password;
    public $token;
    private $sms_sender_name;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->login = Settings::findByKey('sms_login')->value;
        $this->password = Settings::findByKey('sms_password')->value;
        $this->token = Settings::findByKey('sms_token')->value;
        $this->sms_sender_name = Settings::findByKey('sms_sender_name')->value;
    }

    /**
     * @param $phone
     * @param $text
     * @return mixed
     */
    public function send($phone, $text)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://gate.smsaero.ru/send/?user={$this->login}&password={$this->token}&to={$phone}&text={$text}&from={$this->sms_sender_name}&answer=json");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);

//        curl_exec($ch);

        return $result;
    }

    public function checkStatus($sms_id)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://gate.smsaero.ru/status/?user={$this->login}&password={$this->token}&id={$sms_id}&answer=json");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);

        return $result;
    }

    public function testAuthorization()
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://{$this->login}:{$this->token}@gate.smsaero.ru/v2/auth");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        return  curl_exec($ch);
    }
}