<?php

namespace app\services;

use app\models\OrderEvent;
use app\models\Package;
use Yii;
use yii\base\Component;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * Class PackageExcelImporter
 * @package app\services
 *
 * @property int $savedCount Кол-во экспортированных записей
 * @property int $totalCount Кол-во экспортируемых записей
 * @property int $errorCount Кол-во не выгруженных записей
 */
class PackageExcelExporter extends Component
{
    /**
     * @var array Идентификаторы отправлений
     */
    public $package_ids;

    /**
     * @var integer
     */
    private $_savedCount = 0;

    /**
     * @var integer
     */
    private $_totalCount = 0;

    /**
     * @var integer
     */
    private $_errorCount = 0;

    /**
     * @inheritdoc
     */
//    public function init()
//    {
//        \Yii::info(json_decode(json_encode($this), true), 'test');
//        parent::init();
//        if(count($this->package_ids) == 0){
//            throw new InvalidConfigException('Нет выбранных отправлений');
//        }
//    }

    /**
     * @param array $pks Идентификаторы отправлений
     * @return string Путь к сформированному файлу
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function export($pks)
    {
        Yii::info('This is export', 'test');

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        //Заголовки столбцов
        $headers = [
            'track_number' => 'РПО',
            'send_date' => 'Дата отправки',
            'name' => 'ФИО',
            'phone' => 'Телефон',
            'sum' => 'Сумма',
            'product_title' => 'Наименование товара',
            'destination_zip' => 'Индекс адресата',
            'destination_post_address' => 'ОПС адресата',
            'days_back' => 'Дней до возврата',
            'time_days' => 'Дней в пути',
            'short_status' => 'Статус короткий',
            'last_status' => 'Последний статус',
            'zip' => 'Индекс',
            'type' => 'Тип отправления',
            'sender' => 'Отправитель',
            //Деньги получены 1/0
            'payed' => 'Деньги получены',
            'payed_date' => 'Дата получения денег',
            'date_acceptance_transfer' => 'Дата приема перевода',
            'date_payment_transfer' => 'Дата оплаты перевода',
            'reception_zip' => 'Индекс адресата при операции приема', //Наложенный платеж
            'payment_zip' => 'Индекс операции при оплате', //Наложенный платеж
            //История СМС и звонки: запись формата - название шаблона и дата отправки через  разделители (|)
            'history_message' => 'История СМС и звонки',
            'comment' => 'Комментарий',
            'last_status_updated_at' => 'Дата обновления статуса',
            'scheme_id' => 'Схема уведомлений',
        ];

        $row_counter = 1;
        $current_column = 'A';

        foreach ($headers as $header) {
            $sheet->setCellValue($current_column . $row_counter, $header);
            ++$current_column;
        }
        ++$row_counter;
        if ($pks && count($pks) > 0) {
            /** @var Package $package */
            foreach (Package::find()->andWhere(['IN', 'id', $pks])->each() as $package) {
                /** @var OrderEvent $oe_model_type_1 */
                $oe_model_type_1 = OrderEvent::find()->andWhere(['package_id' => $package->id])->andWhere(['type' => 1])->one() ?? ''; //Приём
//                Yii::info($oe_model_type_1->toArray(), 'test');
                /** @var OrderEvent $oe_model_type_3 */
                $oe_model_type_3 = OrderEvent::find()->andWhere(['package_id' => $package->id])->andWhere(['type' => 3])->one() ?? ''; //Оплата
//                Yii::info($oe_model_type_3->toArray(), 'test');
                $current_column = 'A';
                foreach ($headers as $key => $header) {
                    if ($key == 'history_message') {
                        //Получаем историю сообщений для почтового отправления
                        $sheet->setCellValue($current_column . $row_counter, $package->getMessageHistory());
                    } elseif ($key == 'short_status') {
                        $short_status = Package::getShortStatusById($package->$key);
                        $sheet->setCellValue($current_column . $row_counter, $short_status);
                    } elseif ($key == 'last_status') {
                        $last_status = $package->status;
                        $sheet->setCellValue($current_column . $row_counter, $last_status);
                    } elseif ($key == 'type') {
                        $type = $package->info->type;
                        $sheet->setCellValue($current_column . $row_counter, $type);
                    } elseif($key == 'date_acceptance_transfer'){
                        $sheet->setCellValue($current_column . $row_counter, $oe_model_type_1->date ?? '');
                    } elseif($key == 'date_payment_transfer') {
                        $sheet->setCellValue($current_column . $row_counter, $oe_model_type_3->date ?? '');
                    } elseif ($key == 'reception_zip'){
                        $sheet->setCellValue($current_column . $row_counter, $oe_model_type_1->index_to ?? '');
                    } elseif ($key == 'payment_zip'){
                        $sheet->setCellValue($current_column . $row_counter, $oe_model_type_3->index ?? '');
                    } elseif ($key == 'last_status_updated_at'){
                        $sheet->setCellValue($current_column . $row_counter, Yii::$app->formatter->asDatetime($package->$key) ?? '');
                    } elseif ($key == 'scheme_id'){
                        $sheet->setCellValue($current_column . $row_counter, $package->scheme->name ?? '');
                    } else {
                        try {
                            $sheet->setCellValue($current_column . $row_counter, $package->$key);
                        } catch (\Exception $e) {
                            $sheet->setCellValue($current_column . $row_counter, 'Ошибка экспорта');
                        }
                    }
                    ++$current_column;
                }
                ++$row_counter;
            }
        }
        if (!is_dir('tmp')){
            mkdir('tmp', 0777);
        }
        $path = 'tmp/export_' . (int)time() . '.xlsx';
        $writer = new Xlsx($spreadsheet);
        $writer->save($path);
        return $path;
    }

    /**
     * @return int
     */
    public function getSavedCount()
    {
        return $this->_savedCount;
    }

    /**
     * @return int
     */
    public function getTotalCount()
    {
        return $this->_totalCount;
    }

    /**
     * @return int
     */
    public function getErrorCount()
    {
        return $this->_errorCount;
    }
}