<?php

namespace app\services;

use app\models\Settings;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

/**
 * Class VoiceTelService
 * @package app\services
 */
class VoiceTelService extends Component
{
    public $apiKey = '';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if ($this->apiKey == null) {
            $setting = Settings::findByKey('tel_token');
            if ($setting == null) {
                throw new InvalidConfigException('empty tel_token setting. apiKey must be required');
            }
            $this->apiKey = $setting->value;
        }
    }

    public function send($phone, $text)
    {
        $ch = curl_init();

        $data = [
            'apiKey' => $this->apiKey,
            'phone' => str_replace('+', '', $phone),
            'dutyPhone' => 1,
            'record' => [
                'text' => $text,
                'gender' => 0,
            ],
        ];

        Yii::info($data, 'test');

        $data = json_encode($data);

        curl_setopt($ch, CURLOPT_URL, "https://lk.zvonobot.ru/apiCalls/create");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'accept: application/json'));

        $result = curl_exec($ch);

        return $result;
    }

    /**
     * Запрашивает статус звонка
     * @param string $call_id Идентификатор звонка
     *
     * Статусы звонка:
     * processed - в процессе обработки
     * created - создан, но не начат
     * finished - закончен и разговор состоялся
     * canceled - закончен и разговор не состоялся
     * answered - в процессе разговора
     * busy - закончен и абонент занят
     * started - начат
     *
     * @return mixed|string
     */
    public function checkStatus($call_id)
    {
        $data = [
            'apiKey' => $this->apiKey,
        ];


        $result = '';

        $url = 'https://lk.zvonobot.ru/apiCalls/get?apiCallIdList[]=' . $call_id;

        Yii::info('URl: ' . $url, 'test');

        $json = json_encode($data);
        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'accept: application/json'));
            curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
            $result = curl_exec($curl);
            curl_close($curl);
        }

        return $result;
    }

}