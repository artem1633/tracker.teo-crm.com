<?php

namespace app\services;

use app\models\Package;
use PhpOffice\PhpSpreadsheet\IOFactory;
use yii\base\Component;
use yii\base\InvalidConfigException;

/**
 * Class PackageExcelImporter
 * @package app\services
 *
 * @property int $savedCount Кол-во сохраненных записей
 * @property int $totalCount Кол-во загружаемых записей
 * @property int $errorCount Кол-во не сохраненных записей
 * @property array $errors Массив с ошибками
 */
class PackageExcelImporter extends Component
{
    /**
     * @var string Путь до файла
     */
    public $sourceFile;

    /**
     * @var integer
     */
    private $_savedCount = 0;

    /**
     * @var integer
     */
    private $_totalCount = 0;

    /**
     * @var integer
     */
    private $_errorCount = 0;

    private $_errors = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if($this->sourceFile == null){
            throw new InvalidConfigException('sourceFile must be required');
        }
    }

    /**
     * @return void
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function import()
    {
        $inputFileType = IOFactory::identify($this->sourceFile);

        $reader = IOFactory::createReader($inputFileType);

        $sheet = $reader->load($this->sourceFile);
        $sheet = $sheet->getActiveSheet();

        for ($i = 2; $i < 1000; $i++)
        {
            $cellName = "A{$i}";
            $value = $sheet->getCell($cellName)->getValue();
            if($value != '')
            {
                $package = new Package([
                    'track_number' => explode('.', $sheet->getCell("A{$i}")->getValue())[0],
                    'phone' =>'+' . explode('.', $sheet->getCell("C{$i}")->getValue())[0],
                    'name' => $sheet->getCell("D{$i}")->getValue(),
                    'product_title' => $sheet->getCell("E{$i}")->getValue(),
                ]);

                \Yii::info($package->toArray(), 'test');

                if (!$result = $package->save()){
                    \Yii::error($package->errors, '_error');
                    foreach ($package->errors as $field => $error){
                        array_push($this->_errors, $field . ' - ' . $error[0]);
                    }
                    $this->_errorCount++;
                } else {
                    $this->_savedCount++;
                }
                $this->_totalCount++;
            }
        }
    }

    /**
     * @return int
     */
    public function getSavedCount()
    {
        return $this->_savedCount;
    }

    /**
     * @return int
     */
    public function getTotalCount()
    {
        return $this->_totalCount;
    }

    /**
     * @return int
     */
    public function getErrorCount()
    {
        return $this->_errorCount;
    }

    public function getErrors()
    {
        return $this->_errors;
    }
}