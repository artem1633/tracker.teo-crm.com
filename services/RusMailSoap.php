<?php

namespace app\services;

use SoapClient;
use SoapParam;
use Yii;

/**
 * Class RusMailSoap
 * @package app\services
 *
 * Данный класс предоставляет возможность общаться по протоколу SOAP с API Почты России
 */
class RusMailSoap
{
    const WSDL = 'https://tracking.russianpost.ru/rtm34?wsdl';
    const NAMESPACE_DATA = "http://russianpost.org/operationhistory/data";
    const NAMESPACE_DATA1 = "http://www.russianpost.org/RTM/DataExchangeESPP/Data";
    private $login;
    private $password;

    /**
     * RusMailSoap constructor.
     * @param string $login
     * @param string $password
     */
    public function __construct($login, $password)
    {
        $this->login = $login;
        $this->password = $password;
    }

    /**
     * Метод getOperationHistory используется для получения информации о конкретном отправлении.
     * https://tracking.pochta.ru/main
     *
     * @param string $barcode
     * Идентификатор регистрируемого почтового отправления
     *
     * @param string $messageType
     * Тип сообщения
     *
     * @param string $language
     * Язык, на котором должны возвращаться названия операций/атрибутов и сообщения об ошибках
     * @return \stdClass|null
     */
    public function getOperationHistory($barcode = 'RA644000001RU', $messageType = '0', $language = 'RUS')
    {
        $result = (object)[];
        try {
            $client2 = new SoapClient(self::WSDL, array('trace' => 1, 'soap_version' => SOAP_1_2));


            $params3 = array(
                'OperationHistoryRequest' => array(
                    'Barcode' => $barcode,
                    'MessageType' => $messageType,
                    'Language' => $language
                ),
                'AuthorizationHeader' => array(
                    'login' => $this->login,
                    'password' => $this->password,
                    'mustUnderstand ' => 1
                )
            );

//            Yii::info('Параметры запроса:', 'test');
//            Yii::info($params3, 'test');

            $result = $client2->getOperationHistory(new SoapParam($params3, 'OperationHistoryRequest'));

//            Yii::info('Результат запроса:', 'test');
//            Yii::info(json_decode(json_encode($result), true), 'test');

            //Проверяем ответ. Если только одно событие, то почта присылает не массив массивов с событиями, а просто массив с событием
            if ($result && isset($result->OperationHistoryData->historyRecord->AddressParameters)) {
                $history_records[] = json_decode(json_encode($result->OperationHistoryData->historyRecord), true);
                $result->OperationHistoryData->historyRecord = (object)$history_records;
            }
            return $result;
        } catch (\Exception $e) {
            Yii::error($e->getMessage(), '_error');
        }
        return $result;
    }

    /**
     * @param $rpo
     * @param string $lang
     * @return array
     */
    public function getPostalOrderEvents($rpo, $lang = 'RUS')
    {
        $request = <<<XML
            <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" 
                        xmlns:oper="http://russianpost.org/operationhistory" 
                        xmlns:data="http://russianpost.org/operationhistory/data" 
                        xmlns:data1="http://www.russianpost.org/RTM/DataExchangeESPP/Data" 
                        xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" >
                <soap:Header/>
                <soap:Body>
                <oper:PostalOrderEventsForMail>
                    <data:AuthorizationHeader soapenv:mustUnderstand="1">
                        <data:login>$this->login</data:login>
                        <data:password>$this->password</data:password>
                    </data:AuthorizationHeader>
                    <data1:PostalOrderEventsForMailInput Barcode="$rpo" Language="$lang"/>
                    </oper:PostalOrderEventsForMail>
                </soap:Body>
            </soap:Envelope>
XML;

        $client = new SoapClient("https://tracking.russianpost.ru/rtm34?wsdl",
            ['trace' => 1, 'soap_version' => SOAP_1_2]);

        $response = $client->__doRequest($request, "https://tracking.russianpost.ru/rtm34", "PostalOrderEventsForMail",
            SOAP_1_2);

        $p = xml_parser_create();
        xml_parse_into_struct($p, $response, $vals, $index);
        xml_parser_free($p);

//        Yii::info($vals, 'test');
//        VarDumper::dump($vals, 20, true);

        $payment_info = [];

        foreach ($vals as $value) {
            if ($value['tag'] == 'POSTALORDEREVENT') {
                array_push($payment_info, $value['attributes']);
            }
        }
//        Yii::info(json_decode(json_encode($payment_info), true), 'test');

        return $payment_info;
    }


}