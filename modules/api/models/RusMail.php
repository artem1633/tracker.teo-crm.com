<?php

namespace app\modules\api\models;

use app\models\Package;
use app\models\Settings;
use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property string $key Ключ
 * @property string $value Значение
 * @property string $label Комментарий
 */
class RusMail
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [


        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [

        ];
    }

    /**
     * Проверяет проводить или нет обновление статусов
     * @return bool
     */
    public static function update(): bool
    {
        $update_period = Settings::getValueByKey('update_status_period'); //Период обновления статусов отправления (час.)

        Yii::info('Период обновления(час): ' . $update_period, 'test');

        $min_date_time = date('Y-m-d H:i:s',
            time() - ($update_period * 60 * 60)); //минимаьная дата обновления статуса, если меньше - обновляем

        //Получаем все отправления с датой обновления меньше минимальной даты
        /** @var Package $last_update_package */
        $last_update_package = Package::find()
                ->andWhere([
                    'OR',
                    ['NOT IN', 'short_status', [2, 4]],
                    ['IS', 'status', null]
                ])
                ->andWhere(['<', 'last_status_updated_at', $min_date_time])->one() ?? null;
        if ($last_update_package){
            $last_update = $last_update_package->last_status_updated_at;

            \Yii::info('Дата последнего обновления: ' . $last_update, 'test');
            \Yii::info('Сейчас: ' . date('Y-m-d H:i:s', time()), 'test');

            $date_diff = time() - strtotime($last_update);
//            \Yii::info('Time: ' . time(), 'test');
//            \Yii::info('Last Update: ' . strtotime($last_update), 'test');

            \Yii::info('Разница между сейчас и датой последнего обновления (мин): ' . round($date_diff / 60, 2), 'test');

            if ($date_diff < ($update_period * 60 * 60)) {
                \Yii::info('Отмена проверки статуса', 'test');
                return false;
            }
            return true;
        }
        return false;

    }



}
