<?php

namespace app\modules\api\models;

use app\models\forms\MakeCallForm;
use app\models\Log;
use app\models\Package;
use app\models\PackagePhonesSms;
use Yii;
use yii\web\Response;

/**
 * This is the model class for table "settings".
 *
 */
class Message
{

    const TYPE_SMS = 'sms';
    const TYPE_CALL = 'call';

    /**
     * Проверяет (и меняет) статусы незавершенных звонков
     *
     * return Array;
     */
    public static function checkCalls()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $success = 1;
        $data = [];

        /** @var PackagePhonesSms $call */
        foreach (PackagePhonesSms::find()
                     ->andWhere(['IS', 'sms_id', null])
                     ->andWhere(['OR', ['NOT IN', 'status', ['finished', 'canceled', 'busy']], ['IS', 'status', null]])
                     ->each() as $call) {

            Yii::info($call->toArray(), 'test');

            $result = $call->checkCall($call->call_id);

            if (!$result['success']) {
                $success = 0;
                $data[$call->call_id] = $result['data'];
            }
        }
        return ['success' => $success, 'data' => $data];
    }

    /**
     * Проверяет (и меняет) статусы "незавершенных" СМС сообщений
     * @return array
     */
    public static function checkSms()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $success = 1;
        $data = [];

        $excluded_statuses = [
            'delivery success',
            'delivery failure',
        ];
//        Log::log('Начало проверки смс сообщений.', Log::LOG_TYPE_CHECK_SMS);

        /** @var PackagePhonesSms $sms */
        foreach (PackagePhonesSms::find()
                     ->andWhere(['IS', 'call_id', null])
                     ->andWhere(['OR', ['NOT IN', 'status', $excluded_statuses], ['IS', 'status', null]])
                     ->each() as $sms) {

            Log::log('СМС. Номер: ' . $sms->phone, Log::LOG_TYPE_CHECK_SMS);

            Yii::info($sms->sms_id, 'test');


            $result = $sms->checkSms($sms->sms_id);

            if (!$result['success']) {
                $success = 0;
                $data[$sms->sms_id] = $result['data'];
            }

        }
//        Log::log('Окончание проверки смс сообщений.', Log::LOG_TYPE_CHECK_SMS);

        return ['success' => $success, 'data' => $data];
    }

    /**
     * Ищет и проверяте отправления, по которым нужен повторный обзвон
     * @return void
     */
    public function checkRecall()
    {
        //Выбираем последние звонки для каждой посылки
        $sub_query = PackagePhonesSms::find()
            ->select(['MAX(datetime_send)'])
            ->groupBy('package_id');

        $package_messages = PackagePhonesSms::find()
            ->select(['package_id', 'status', 'template_id', 'datetime_send'])
            ->andWhere(['IS', 'sms_id', null])
            ->andWhere(['IN', 'datetime_send', $sub_query]);

        /** @var PackagePhonesSms $message */
        foreach ($package_messages->each() as $message) {
            if (!$this->callProceed($message)){
                Yii::error('Ошибка обработки повторного звонка. ID звонка: ' . $message->id, '_error');
            }
        }
    }

    /**
     * Отбор по типу сообщения и отправка сообщения
     * @param string $phone Телефон клиента
     * @param string $text Текст сообщения
     * @param string $type Тип сообщения (sms/call)
     * @return bool|string
     */
    public function sendNotify($phone, $text, $type)
    {
        if (!$phone || !$text || !$type) return false;

        if ($type == self::TYPE_SMS) {
           return Yii::$app->smsService->send($phone, $text);
        } elseif ($type == self::TYPE_CALL) {
            return Yii::$app->voiceService->send($phone, $text);
        }

        return '';
    }

    /**
     * Проверка звонка для повторного обзвона
     * @param PackagePhonesSms $message
     * @return bool
     */
    public function callProceed(PackagePhonesSms $message)
    {
        Yii::info($message->toArray(), 'test');

        if ($message->status == 'canceled' || $message->status == 'busy') {
            $package = Package::findOne($message->package_id);
            //Проверяем условия
            if ($package->preCheckCall()) {
                //Звоним повторно
                $model = new MakeCallForm([
                    'packageId' => $package->id,
                    'templateId' => $message->template_id
                ]);
                $create_result = $model->make();
                if (!$create_result['success']) {
                    Yii::error($create_result['data'], '_error');
                    return false;
                } else {
                    //После звонка записываем кол-во совершенных звонков адресату и дату последнего звонка
                    $package->num_re_call_finished += 1;
                    $package->date_re_call = date('Y-m-d H:i:s', time());

                    if (!$package->save()) {
                        Yii::error($package->errors, '_error');
                        return false;
                    }
                }
            } else {
                Yii::error($package->check_error, '_error');
            }
        }
        return true;
    }
}
