<?php

namespace app\modules\api\controllers;

use app\models\Log;
use app\models\NoticeToPackage;
use app\models\Package;
use app\modules\api\models\Message;
use yii\rest\ActiveController;
use yii\web\Response;

/**
 * Class MessageController
 * @package app\modules\api\controllers
 */
class MessageController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * Запрашивает и обновляет статусы СМС и телефонных сообщений
     */
    public function actionUpdateStatus()
    {
        Message::checkCalls();
        Message::checkSms();
        //Проверяем посылки по которым нет дозвона
        (new Message)->checkRecall();
    }

    /**
     * Проверяет статусы звонков
     */
    public function actionCheckCalls()
    {
        Message::checkCalls();
    }

    /**
     * Проверяет статусы СМС
     */
    public function actionCheckSms()
    {
        Message::checkSms();
    }

    /**
     * Проверяет  и если необходимо создает повторный звонок
     */
    public function actionCheckRecall()
    {
        (new Message)->checkRecall();
    }

    /**
     * Уведомления получателей по схеме оповещения
     */
    public function actionNotifyByScheme()
    {
        set_time_limit(900);
        ini_set('memory_limit', '2048M');

        Log::log('Начало проверки оповещений. api/message/notify-by-scheme');

       $notify_result = (new NoticeToPackage())->notifyByScheme();

        Log::log('Завершение проверки оповещений. api/message/notify-by-scheme');

        $package = new Package();
        //Удаляем из посылок отработанные схемы
        $clean_result = $package->cleanSchemes();

        //Проверяем наличие связей посылки с уведомлениями

        $check_relations_result = $package->checkNotifyRelations();

        \Yii::info($check_relations_result, 'test');

        return [$notify_result, $clean_result];
    }
}