<?php

namespace app\modules\api\controllers;

use app\models\Package;
use app\models\Settings;
use Yii;
use yii\rest\ActiveController;
use yii\web\Response;

/**
 * Class RusMailController
 * @package app\modules\api\controllers
 */
class RusMailController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * Обновляет статусы посылок.
     */
    public function actionUpdateStatus()
    {
        set_time_limit(599);
        \Yii::info('Начало проверки статусов', 'test');

        $timeRange = date('Y-m-d H:i:s', time() - 60 * 60 * 24 * 90);
        $period = Settings::getValueByKey('update_status_period');
        $min_time = date('Y-m-d H:i:s', time() - ($period * 60 * 60));

        //Пишем логин и пароль от ПР для уменьшения кол-ва запросов
        Yii::$app->session->set('pr_login', Settings::getValueByKey('mail_login'));
        Yii::$app->session->set('pr_pass', Settings::getValueByKey('mail_password'));

        $sql = <<<SQL
                SELECT `package`.* 
                FROM `package` 
                LEFT JOIN `order_event` `oe` 
                  ON `package`.`id` = `oe`.`package_id`
                WHERE (`oe`.`id` IN (SELECT `id` FROM `order_event` WHERE `type` <> 3))
                  AND (`send_date` > :date_range) 
                  AND ((`short_status` NOT IN (4, 8)) OR (`status` IS NULL))
                  AND `last_status_updated_at` < :min_time
                  ORDER BY `last_status_updated_at` ASC 
                LIMIT 150
SQL;
        $package_statuses_width_oe = Package::findBySql($sql,
                [':date_range' => $timeRange, ':min_time' => $min_time])->all() ?? null;

        if ($package_statuses_width_oe) {
            Yii::info('Записей для обновления отправлений с наложенным платежом: ' . count($package_statuses_width_oe),
                'test');
            /** @var Package $package */
            foreach ($package_statuses_width_oe as $package) {
                Yii::info('Обновляем статус РПО: ' . $package->track_number, 'test');
                $package->updateStatus();
            }
            Yii::info('Обновление статусов для отправлений с наложенным платежом проведено успешно', 'test');
        } else {
            Yii::info('Записей для обновления отправлений с наложенным платежом не найдено', 'test');
        }


        $sql2 = <<<SQL
                SELECT `package`.* 
                FROM `package` 
                  WHERE (`send_date` > :date_range) 
                  AND ((`short_status` NOT IN (4, 8)) OR (`status` IS NULL))
                  AND `last_status_updated_at` < :min_time
                  ORDER BY `last_status_updated_at` ASC 
                  LIMIT 150
SQL;

        $packages = Package::findBySql($sql2, [':date_range' => $timeRange, ':min_time' => $min_time])->all() ?? null;
        if ($packages) {
            Yii::info('Записей для обновления: ' . count($packages), 'test');
            /** @var Package $package */
            foreach ($packages as $package) {
//                    Yii::info('Обновляем статус РПО: ' . $package->track_number, 'test');
                $package->updateStatus();
            }
            Yii::info('Обновление статусов проведено успешно', 'test');
        } else {
            Yii::info('Записей для обновления не найдено', 'test');
        }
        echo 'Обновление проведено успешно';

        Yii::$app->session->remove('pr_login');
        Yii::$app->session->remove('pr_pass');
    }
}