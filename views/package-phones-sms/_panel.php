<?php

use kartik\date\DatePicker;
use yii\helpers\Html;

/** @var array $filter Значения фильтра */

Yii::info($filter, 'test');

?>
<div class="panel-before-package">
    <div class="row">
        <div class="col-xs-8">
            <?=
            Html::a('<i class="fa fa-repeat"></i>', ['update-statuses'],
                [
                    'data-pjax' => 1,
                    'class' => 'btn btn-white',
                    'title' => 'Обновить статусы сообщений'
                ])
            ?>
        </div>
        <div class="col-xs-4">
            <div class="filter-date">
                <label for=".input-group">Дата</label>
                <div class="input-group input-group-sm">
                    <span class="input-group-addon">C:</span>
                    <?php
                    try {
                        echo DatePicker::widget([
                            'name' => 'start-date',
                            'type' => DatePicker::TYPE_INPUT,
                            'value' => $filter['date_period']['start'],
                            'pluginOptions' => [
                                'todayHighlight' => true,
                                'autoclose' => true,
                            ],
                            'options' => [
                                'id' => 'date-start',
                                'class' => 'form-control',
                                'autocomplete' => "off",
                            ]
                        ]);
                    } catch (Exception $e) {
                        Yii::error($e->getMessage(), '_error');
                    } ?>
                    <span class="input-group-addon">ПО:</span>
                    <?php
                    try {
                        echo DatePicker::widget([
                            'name' => 'start-date',
                            'type' => DatePicker::TYPE_INPUT,
                            'value' => $filter['date_period']['end'],
                            'pluginOptions' => [
                                'todayHighlight' => true,
                                'autoclose' => true,
                            ],
                            'options' => [
                                'id' => 'date-end',
                                'class' => 'form-control',
                                'autocomplete' => "off",
                            ]
                        ]);
                    } catch (Exception $e) {
                        Yii::error($e->getMessage(), '_error');
                    } ?>
                    <span class="input-group-btn">
                        <?= Html::a('<i class="fa fa-search"></i>', ['/package-phones-sms/find-by-period'], [
                            'id' => 'search-period-btn',
                            'class' => 'btn btn-default'
                        ]) ?>
                    </span>
                    <span class="clear-btn input-group-btn">
                       <?= Html::a('<i class="glyphicon glyphicon-remove"></i>', ['package-phones-sms/find-by-period'], [
                           'id' => 'clear-stat-filter',
                           'title' => 'Очистить фильтр',
                           'class' => 'btn btn-default'
                       ]) ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

