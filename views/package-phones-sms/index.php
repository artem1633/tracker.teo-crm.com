<?php

use johnitvn\ajaxcrud\BulkButtonWidget;
use johnitvn\ajaxcrud\CrudAsset;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PackagePhonesSmsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сообщения';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="panel panel-inverse package-phones-sms-index">
    <div class="panel-heading">
        <!--        <div class="panel-heading-btn">-->
        <!--        </div>-->
        <h4 class="panel-title"><?= $this->title;?></h4>
    </div>
    <?php require (__DIR__ . '/_panel.php')?>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">
            <?php
            try {
                echo GridView::widget([
                    'id' => 'crud-datatable',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => ['class' => 'table table-bordered'],
                    'pjax' => true,
                    'columns' => require(__DIR__ . '/_columns.php'),
                    'panelBeforeTemplate' => '',
//                        Html::a('Добавить <i class="fa fa-plus"></i>', ['create'],
//                            [
//                                'role' => 'modal-remote',
//                                'title' => 'Добавить пользователя',
//                                'class' => 'btn btn-success'
//                            ]) . '&nbsp;' .
//                        Html::a('<i class="fa fa-repeat"></i>', ['update-statuses'],
//                            ['data-pjax' => 1, 'class' => 'btn btn-white', 'title' => 'Обновить']),
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'panel' => [
                        'headingOptions' => ['style' => 'display: none;'],
                        'after' => BulkButtonWidget::widget([
                                'buttons' => Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить все',
                                    ["bulk-delete"],
                                    [
                                        "class" => "btn btn-danger btn-xs",
                                        'role' => 'modal-remote-bulk',
                                        'data-confirm' => false,
                                        'data-method' => false,// for overide yii data api
                                        'data-request-method' => 'post',
                                        'data-confirm-title' => 'Вы уверены?',
                                        'data-confirm-message' => 'Подтвердите удаление элемента'
                                    ]),
                            ]) .
                            '<div class="clearfix"></div>',
                    ]
                ]);
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
            } ?>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
<?php
$script = <<<JS
$(document).ready(function () {
    $(document).on('click', '#search-period-btn', function(e) {
        e.preventDefault();
        send($(this));
        // var href = $(this).attr('href');
        // var start = $('#date-start').val();
        // var end = $('#date-end').val();
        // $.get(
        //     href,
        //     {
        //         start: start,
        //         end: end
        //     }
        // )
    });
    
    function send(obj){
        var href = obj.attr('href');
        var start = $('#date-start').val();
        var end = $('#date-end').val();
        $.get(
            href,
            {
                start: start,
                end: end
            }
        )
    }
    
    $(document).on('click', '.clear-btn', function(e) {
        e.preventDefault();
        $('#date-start, #date-end').val('');

        send($(this).find('a'));
        // var href = $(this).find('a').attr('href');
        // var start = $('#date-start').val();
        // var end = $('#date-end').val();
        //  $.get(
        //     href,
        //     {
        //         start: start,
        //         end: end
        //     }
        // )
    })
});
JS;
$this->registerJs($script);