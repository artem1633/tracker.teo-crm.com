<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PackagePhonesSms */
?>
<div class="package-phones-sms-view">
 
    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'id',
                    'label' => '#',
                    'contentOptions' => [
                        'width' => '50px',
                        'class' => 'text-center',
                        'style' => 'vertical-align: middle'
                    ],
                ],
                'sms_id',
                'datetime_send:datetime',
                'datetime_deliver:datetime',
                'package_id',
                'phone',
                'status',
                'text:ntext',
//                'created_at',
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
    } ?>

</div>
