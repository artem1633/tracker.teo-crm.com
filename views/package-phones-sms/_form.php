<?php

use app\models\Package;
use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\PackagePhonesSms */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="package-phones-sms-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sms_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'package_id')->dropDownList(Package::getList(), [
        'prompt' => 'Выберите посылку'
    ]) ?>

    <?= $form->field($model, 'phone')->widget(MaskedInput::className(), [
        'mask' => '+7-999-999-99-99',
    ]) ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'datetime_send')->widget(DateTimePicker::className(), [
        'pluginOptions' => [
            'autoclose' => true,
        ]
    ]) ?>
    <?= $form->field($model, 'datetime_deliver')->widget(DateTimePicker::className(), [
        'pluginOptions' => [
            'autoclose' => true,
        ]
    ]) ?>
    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
