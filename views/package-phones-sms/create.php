<?php

/* @var $this yii\web\View */
/* @var $model app\models\PackagePhonesSms */

?>
<div class="package-phones-sms-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
