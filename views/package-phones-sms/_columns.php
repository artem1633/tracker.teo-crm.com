<?php

use app\models\PackagePhonesSms;
use app\models\SmsTemplate;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'type',
        'value' => function (PackagePhonesSms $model) {
            if ($model->sms_id ?? '') {
                return 'СМС';
            } elseif ($model->call_id ?? '') {
                return 'Тел. звонок';
            }
            return null;
        },
        'label' => 'Тип',
        'filter' => SmsTemplate::getTypeList(),
    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'message_id',
//        'value' => function (PackagePhonesSms $model) {
//            if ($model->sms_id ?? '') {
//                return $model->sms_id;
//            } elseif ($model->call_id ?? '') {
//                return $model->call_id;
//            }
//            return null;
//        }
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'package_id',

    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'phone',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'text',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'status',
        'value' => function (PackagePhonesSms $model) {
            if ($model->status) {
                if ($model->call_id) {
                    return $model->getCallStatusName($model->status) ?? null;
                } elseif ($model->sms_id) {
                    return $model->getSmsStatusName($model->status) ?? null;
                }
            }
            return null;
        }
    ],

    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'datetime_send',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'datetime_deliver',
        'format' => 'datetime',
        'value' => function(PackagePhonesSms $model){
            if (!$model->datetime_deliver || $model->datetime_send == ''){
                return $model->datetime_send;
            } elseif (!$model->datetime_deliver && !$model->datetime_send == ''){
                return $model->created_at;
            }
            return $model->datetime_deliver;
        }
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created_at',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'template' => '{check-status} {update} {delete}',
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role' => 'modal-remote',
                    'title' => 'Удалить',
                    'data-confirm' => false,
                    'data-method' => false,// for overide yii data api
                    'data-request-method' => 'post',
                    'data-confirm-title' => 'Вы уверены?',
                    'data-confirm-message' => 'Вы действительно хотите удалить данное сообщение?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                    'role' => 'modal-remote',
                    'title' => 'Изменить',
                    'data-confirm' => false,
                    'data-method' => false,// for overide yii data api
                    'data-request-method' => 'post',
                ]);
            },
            'check-status' => function ($url, $model) {
                return Html::a('<i class="fa fa-refresh text-info" style="font-size: 16px;"></i>', $url, [
                    'role' => 'modal-remote',
                    'title' => 'Проверить статус',
                    'data-confirm' => false,
                    'data-method' => false,// for overide yii data api
                    'data-request-method' => 'post',
                ]);
            },
        ],
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Редактировать', 'data-toggle' => 'tooltip'],
    ],

];   