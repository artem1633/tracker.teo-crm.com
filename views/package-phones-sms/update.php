<?php

/* @var $this yii\web\View */
/* @var $model app\models\PackagePhonesSms */
?>
<div class="package-phones-sms-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
