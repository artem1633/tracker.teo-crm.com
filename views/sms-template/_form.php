<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SmsTemplate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sms-template-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'content')->textarea([
                'rows' => 6,
                'placeholder' => 'Например: Ваше отправление номер {НомерРПО} ожидает вручения по адресу - {АдресОтделения}'
            ]) ?>

            <?= $form->field($model, 'type')->hiddenInput()->label(false) ?>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <p class="text-center">Для подстановки параметров используйте следующие тэги:</p>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <ul>
                        <li>НомерРПО</li>
                        <li>ФИО</li>
                        <li>НаименованиеТовара</li>
                        <li>Сумма</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul>
                        <li>Индекс</li>
                        <li>АдресОтделения</li>
                        <li>ДнейДоВозврата</li>
                    </ul>
                </div>
            </div>

        </div>
    </div>


    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
