<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Scheme */
?>
<div class="scheme-view">
 
    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'description:ntext',
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

</div>
