<?php

use app\models\Scheme;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'name',
    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'type',
//        'filter' => [
//            Scheme::TYPE_SMS => (new Scheme())->getTypeNameByKey(Scheme::TYPE_SMS),
//            Scheme::TYPE_CALL => (new Scheme())->getTypeNameByKey(Scheme::TYPE_CALL),
//        ],
//        'value' => function (Scheme $model) {
//            return $model->getTypeNameByKey($model->type);
//        }
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'description',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'template' => '{configure} {view-structure} {update} {delete}',
        'buttons' => [
            'configure' => function ($url, Scheme $model) {
                return Html::a('<span class="glyphicon glyphicon-cog"></span>',
                    ['/scheme/configure', 'id' => $model->id], [
                        'title' => 'Конфигурация схемы',
                        'role' => 'modal-remote',
                        'data-pjax' => 1,
                    ]);
            },
            'view-structure' => function ($url, Scheme $model) {
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                    ['/notice/index', 'scheme_id' => $model->id], [
                        'title' => 'Просмотр структуры схемы',
                        'data-pjax' => 0,
                    ]);
            }
        ],
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },

        'viewOptions' => ['role' => 'modal-remote', 'title' => 'Просмотр уведомлений', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Редактировать', 'data-toggle' => 'tooltip'],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Delete',
            'data-confirm' => false,
            'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Вы уверены?',
            'data-confirm-message' => 'Подтвердите удаление элемента'
        ],
    ],

];   