<?php

/* @var $this yii\web\View */
/* @var $model app\models\Scheme */
?>
<div class="scheme-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
