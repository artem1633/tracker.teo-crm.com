<?php

use app\models\SmsTemplate;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $scheme app\models\Scheme */
/* @var $notices app\models\Notice[] */

$scheme->templates = [1, 2, 3];

Yii::info($scheme->templates, 'test');

?>
<?php $form = ActiveForm::begin(); ?>

    <div class="scheme-configure">
        <?php foreach ($notices as $notice): ?>
            <div class="configure-panel">
                <div class="row">
                    <div class="col-md-6">
                        <?= $notice->name; ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($notice, 'template_id')->dropDownList(SmsTemplate::getAllTemplates(), [
                            'name' => 'template_id-' . $notice->id,
                            'prompt' => 'Не уведомлять',
                        ])->label(false) ?>
                    </div>
                    <div class="params">
                        <?php if ($notice->interval): ?>
                            <div class="col-md-6">
                                <?= $form->field($notice, 'interval')->textInput([
                                    'name' => 'interval-' . $notice->id,
                                    'prompt' => '0',
                                ]) ?>
                            </div>
                        <?php endif; ?>
                        <?php if ($notice->repeat_num): ?>
                            <div class="col-md-6">
                                <?= $form->field($notice, 'repeat_num')->textInput([
                                    'name' => 'repeat_num-' . $notice->id,
                                    'prompt' => '0',
                                ]) ?>
                            </div>
                        <?php endif; ?>
                        <?php if ($notice->day_to_return): ?>
                            <div class="col-md-6">
                                <?= $form->field($notice, 'day_to_return')->textInput([
                                    'name' => 'day_to_return-' . $notice->id,
                                    'prompt' => '0',
                                ]) ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

<?php //echo $form->field($notice, 'name')->textInput(['maxlength' => true]) ?>
<?php ActiveForm::end(); ?>

<?php
//$script = <<<JS
//$(document).ready(function() {
//    prepare();
//
//    function prepare() {
//
//    }
//})
//JS;
