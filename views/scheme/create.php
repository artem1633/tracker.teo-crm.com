<?php

/* @var $this yii\web\View */
/* @var $model app\models\Scheme */

?>
<div class="scheme-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
