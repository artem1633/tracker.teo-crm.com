<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Log */
?>
<div class="log-view">

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'created_at',
                [
                    'attribute' => 'type',
                    'value' => $model->getList()[$model->type],
                ],
                'log:ntext',
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

</div>
