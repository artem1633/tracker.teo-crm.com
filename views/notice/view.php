<?php

use app\models\Notice;
use app\models\Package;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Notice */
?>
<div class="notice-view">

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                [
                    'attribute' => 'scheme_id',
                    'value' => $model->scheme->name ?? null,
                ],
                [
                    'attribute' => 'template_id',
                    'value' => $model->template->name ?? null,
                ],
                [
                    'attribute' => 'short_status',
                    'value' => function(Notice $model){
                        return Package::getShortStatusById($model->short_status) ?? null;
                    },
                ],
                'interval',
                'repeat_num',
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

</div>
