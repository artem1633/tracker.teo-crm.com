<?php

/* @var $this yii\web\View */
/* @var $model app\models\Notice */
?>
<div class="notice-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
