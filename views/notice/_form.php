<?php

use app\models\Package;
use app\models\Scheme;
use app\models\SmsTemplate;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Notice */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="notice-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'scheme_id')->dropDownList((new Scheme())->getList(), [
                'prompt' => 'Выберите схему',
                'disabled' => $model->isNewRecord,
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'type')->dropDownList(SmsTemplate::getTypeList(), [
                'prompt' => 'Выберите тип уведомления',
                'onchange' => "
                    $.get(
                        '/notice/set-type',
                        {type: $(this).val()},
                        function(response){
                        console.log(response)
                            $('#dd-template').removeAttr('disabled')
                            $('#dd-template').html(response);
                        }
                    )
                ",
            ])->label('Тип уведомления') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'template_id')->dropDownList(SmsTemplate::getList($model->scheme->type), [
                'prompt' => 'Выберите шаблон',
                'id' => 'dd-template',
                'disabled' => true,
            ]) ?>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'short_status')->dropDownList(Package::getShortStatusList(), [
                'prompt' => 'Выберите короткий статус',
            ]) ?>
        </div>
    </div>

    <?php
    $int = [];

    for ($i = 0; $i < 11; $i++) {
        $int[$i] = $i;
    }
    ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'interval')->dropDownList($int) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'repeat_num')->dropDownList($int) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'day_to_return')->dropDownList($int) ?>
        </div>
    </div>


    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
