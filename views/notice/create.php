<?php

/* @var $this yii\web\View */
/* @var $model app\models\Notice */

?>
<div class="notice-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
