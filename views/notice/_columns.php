<?php

use app\models\Notice;
use app\models\Package;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'scheme_id',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'template_id',
        'content' => function(Notice $model){
            return $model->template->name ?? null;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'short_status',
        'content' => function(Notice $model){
            return Package::getShortStatusById($model->short_status) ?? null;
        }
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'exec_date',
//    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'interval',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'repeat_num',
     ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'day_to_return',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверены?',
                          'data-confirm-message'=>'Подтвердите удаление элемента'],
    ],

];   