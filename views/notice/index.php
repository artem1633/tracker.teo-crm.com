<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 

/* @var $this yii\web\View */
/* @var $searchModel app\models\NoticeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $scheme \app\models\Scheme */

$this->title = 'Notices';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="notice-index">
    <div id="ajaxCrudDatatable">
        <?php
        try {
            echo GridView::widget([
                'id' => 'crud-datatable',
                'dataProvider' => $dataProvider,
//                'filterModel' => $searchModel,
                'pjax' => true,
                'columns' => require(__DIR__ . '/_columns.php'),
                'toolbar' => [
                    [
                        'content' =>
                            Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create', 'scheme_id' => $scheme->id],
                                [
                                    'role' => 'modal-remote',
                                    'title' => 'Добавить уведомление',
                                    'class' => 'btn btn-default'
                                ]) .
                            '{toggleData}' .
                            '{export}'
                    ],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'panel' => [
                    'type' => 'primary',
                    'heading' => '<i class="glyphicon glyphicon-list"></i> Список уведомлений для схемы "' . $scheme->name . '"',
                    'before' => Html::a('<i class="glyphicon glyphicon-chevron-left"></i>Назад к схемам', ['/scheme'], [
                            'class' => 'btn btn-info'
                    ]),
                ]
            ]);
        } catch (Exception $e) {
            Yii::error($e->getMessage(), '_error');
            echo $e->getMessage();
        } ?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
