<?php

/* @var $this yii\web\View */
/* @var $model app\models\PackageInfo */
?>
<div class="package-info-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
