<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PackageInfo */
?>
<div class="package-info-view">
 
    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'package_id',
                'type',
                'weight',
                'cost',
                'pay_on_delivery',
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
    } ?>

</div>
