<?php

use app\admintheme\widgets\TopMenu;
use app\models\SmsTemplate;
use johnitvn\ajaxcrud\CrudAsset;

CrudAsset::register($this);

?>

<div id="top-menu" class="top-menu">
    <?php if (Yii::$app->user->isGuest == false): ?>
        <?php
        try {
            echo TopMenu::widget(
                [
                    'options' => ['class' => 'nav'],
                    'items' => [
                        ['label' => 'Пользователи', 'icon' => 'fa  fa-user-o', 'url' => ['/user'],],
                        ['label' => 'Посылки', 'icon' => 'fa  fa-archive', 'url' => ['/package'],],
                        ['label' => 'Настройки', 'icon' => 'fa  fa-cog', 'url' => ['/settings'],],
                        [
                            'label' => 'Сообщения',
                            'icon' => 'fa fa-envelope',
                            'options' => ['class' => 'has-sub'],
                            'url' => ['#'],
                            'items' => [
                                [
                                    'label' => 'Шаблоны СМС',
                                    'icon' => 'fa  fa-object-group',
                                    'url' => ['/sms-template', 'SmsTemplateSearch[type]' => SmsTemplate::TEMPLATE_TYPE_SMS],
                                ],
                                [
                                    'label' => 'Шаблоны тел. сообщений',
                                    'icon' => 'fa  fa-object-group',
                                    'url' => ['/sms-template', 'SmsTemplateSearch[type]' => SmsTemplate::TEMPLATE_TYPE_CALL],
                                ],
                                [
                                    'label' => 'Схемы уведомлений',
                                    'icon' => 'fa fa-commenting-o',
                                    'url' => ['/scheme'],
                                ],
//                                [
//                                    'label' => 'Сообщения',
//                                    'icon' => 'fa fa-envelope-square',
//                                    'url' => ['package-phones-sms/index']
//                                ],

                            ]
                        ],
                        ['label' => 'Статистика', 'icon' => 'fa  fa-area-chart', 'url' => ['package-phones-sms/index'],],
                    ],
                ]
            );
        } catch (Exception $e) {
            Yii::error($e->getMessage(), '_error');
        }
        ?>
    <?php endif; ?>
</div>
