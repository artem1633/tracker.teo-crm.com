<?php

/* @var $this yii\web\View */
/* @var $model app\models\OrderEvent */
?>
<div class="order-event-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
