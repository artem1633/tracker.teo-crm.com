<?php

use kartik\date\DatePicker;
use yii\helpers\Html;

/* @var array $filters Значения фильтров */
/* @var string $filter_state Стиль (display) для панели фильтров */
/* @var string $sum Сумма по отобранным строкам */

Yii::info($filters, 'test');
?>
<div class="panel-before-package">
    <div class="btn-filter pull-right">
        <?= Html::button('Фильтры&nbsp;<span id="warning-filter"></span>', [
            'class' => 'btn btn-small btn-primary',
            'id' => 'switch-filter',
            'style' => 'position: absolute; right: 20px; z-index: 10;'
        ]) ?>
    </div>

    <div class="row">
        <div class="filter col-xs-12" style="<?= $filter_state ?>">
            <div class="panel-filters">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <div class="filter-date" style="margin-right: 7%">
                                <label for=".input-group">Дата&nbsp;отправки</label>
                                <div class="input-group input-group-sm">
                                    <span class="input-group-addon">C:</span>
                                    <?php
                                    try {
                                        echo DatePicker::widget([
                                            'name' => 'start-date',
                                            'type' => DatePicker::TYPE_INPUT,
                                            'value' => $filters['date_period']['start'],
                                            'pluginOptions' => [
                                                'todayHighlight' => true,
                                                'autoclose' => true,
                                            ],
                                            'options' => [
                                                'id' => 'date-start',
                                                'class' => 'form-control',
                                                'autocomplete' => "off",
                                            ]
                                        ]);
                                    } catch (Exception $e) {
                                        Yii::error($e->getMessage(), '_error');
                                    } ?>
                                    <span class="input-group-addon">ПО:</span>
                                    <?php
                                    try {
                                        echo DatePicker::widget([
                                            'name' => 'end-date',
                                            'type' => DatePicker::TYPE_INPUT,
                                            'value' => $filters['date_period']['end'],
                                            'pluginOptions' => [
                                                'todayHighlight' => true,
                                                'autoclose' => true,
                                            ],
                                            'options' => [
                                                'id' => 'date-end',
                                                'class' => 'form-control',
                                                'autocomplete' => "off",
                                            ]
                                        ]);
                                    } catch (Exception $e) {
                                        Yii::error($e->getMessage(), '_error');
                                    } ?>
                                    <span class="input-group-btn">
                                <?= Html::a('<i class="fa fa-search"></i>', ['/package/find-by-filter'], [
                                    'id' => 'search-period-btn',
                                    'class' => 'btn btn-default'
                                ]) ?>
                            </span>
                                    <span class="clear-btn input-group-btn">
                                <?= Html::a('<i class="glyphicon glyphicon-remove"></i>', ['/package/find-by-filter'], [
                                    'id' => 'clear-date-filter',
                                    'title' => 'Очистить фильтр "Дата отправки"',
                                    'class' => 'btn btn-default'
                                ]) ?>
                            </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="filter-day-to-back" style="margin-right: 7%">
                                <label for=".input-group">Дней&nbsp;до&nbsp;возврата</label>
                                <div class="input-group input-group-sm">
                                    <span class="input-group-addon">C:</span>
                                    <?= Html::input('text', 'init-num-day', $filters['day_period']['start'], [
                                        'id' => 'init-day',
                                        'class' => 'form-control'
                                    ]) ?>
                                    <span class="input-group-addon">ПО:</span>
                                    <?= Html::input('text', 'final-num-day', $filters['day_period']['end'], [
                                        'id' => 'final-day',
                                        'class' => 'form-control'
                                    ]) ?>
                                    <span class="input-group-btn">
                                <?= Html::a('<i class="fa fa-search"></i>', ['/package/find-by-filter'], [
                                    'id' => 'search-period-btn',
                                    'class' => 'btn btn-default'
                                ]) ?>
                            </span>
                                    <span class="clear-btn input-group-btn">
                                <?= Html::a('<i class="glyphicon glyphicon-remove"></i>', ['/package/find-by-filter'], [
                                    'id' => 'clear-days-filter',
                                    'title' => 'Очистить фильтр "Дней до возврата"',
                                    'class' => 'btn btn-default'
                                ]) ?>
                            </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="material-switch">
                            <input id="show-comment" name="PackageSearch[show_empty_comment]"
                                   type="checkbox"
                                <?php echo $filters['show_comment'] ?? null ?>/>
                            <label for="show-comment" class="label-primary"></label>
                            <p style="margin-left: 10px;">Показать комментарии</p>
                        </div>
                        <div class="material-switch">
                            <input id="show-empty-comment" name="PackageSearch[show_empty_comment]"
                                   type="checkbox"
                                <?php echo $filters['show_empty_comment'] ?? null ?>/>
                            <label for="show-empty-comment" class="label-primary"></label>
                            <p style="margin-left: 10px;">Скрыть комментарии</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <?=
            Html::a('Импорт <span class="fa fa-sign-in"></span>', ['import'], [
                'role' => 'modal-remote',
                'title' => 'Импортировать из Excel',
                'class' => 'btn btn-info'
            ]) . ' ' .
            Html::a('Экспорт <span class="fa fa-sign-out"></span>', ['export-filter'], [
                'id' => 'export-filter-btn',
                'title' => 'Экспортировать в Excel',
                'class' => 'btn btn-primary'
            ]) . ' ' .
            Html::a('Добавить <i class="fa fa-plus"></i>', ['create'],
                [
                    'role' => 'modal-remote',
                    'title' => 'Добавить посылку',
                    'class' => 'btn btn-success'
                ]) . '&nbsp;' .
            Html::a('<i class="fa fa-repeat"></i><span id="progress" style="display: none">  0 из 0</span>',
                ['check-status'],
                [
                    'id' => 'filter-check-status',
//                    'data-pjax' => 1,
                    'class' => 'btn btn-white',
                    'title' => 'Обновить статусы отправлений'
                ])
            ?>
        </div>
    </div>
</div>

