<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var \app\models\forms\SendSmsForm $model
 */

?>

<?php $form = ActiveForm::begin() ?>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-md-12">
            <?= Html::button('Удалить комментарии у выбранных отправлений', [
                'id' => 'remove-comment',
                'class' => 'btn btn-danger btn-block',
                'data-dismiss' => 'modal'
            ])
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'comment')->textarea([
                'rows' => 5,
                'placeholder' => 'Введите комментарий'
            ])->label(false) ?>
        </div>
    </div>

<?php ActiveForm::end() ?>

<?php
$script = <<<JS
$(document).ready(function() {
      $(document).on('click', '#remove-comment', function() {
        $.get(
            '/package/del-comment',
            function(response) {
                if (response['success'] === 1){
                   $('#ajaxCrudModal').fadeOut();
                  location.href = window.location.href;
                } else {
                    $('#remove-scheme').text(response['error']);
                }
              
            }
        )
    });
});

JS;

$this->registerJs($script);