<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Package */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \app\models\forms\SchemeForm */

?>

    <div class="package-form">

        <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-md-12">
                <?= Html::button('Удалить схемы у выбранных отправлений', [
                    'id' => 'remove-scheme',
                    'class' => 'btn btn-danger btn-block',
                    'data-dismiss' => 'modal'
                ])
                ?>
            </div>
        </div>
        <div class="row" style="margin-top: 20px;">
            <div class="col-md-12">
                <?= $form->field($model, 'scheme_id')->dropDownList($model->getSchemeList(), [
                    'prompt' => 'Выберите схему'
                ])->label(false) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

<?php
$script = <<<JS
$(document).ready(function() {
      $(document).on('click', '#remove-scheme', function() {
        $.get(
            '/package/del-scheme',
            function(response) {
                if (response['success'] === 1){
                   $('#ajaxCrudModal').fadeOut();
                  location.href = window.location.href;
                } else {
                    $('#remove-scheme').text(response['error']);
                }
              
            }
        )
    });
});

JS;

$this->registerJs($script);
