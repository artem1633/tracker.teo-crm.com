<?php

use app\components\BulkPackage;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PackageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var double $sum Сумма по всем отобранным записям */

$this->title = "Посылки";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
    <div class="panel panel-inverse package-index">
        <div class="panel-heading">
            <!--        <div class="panel-heading-btn">-->
            <!--        </div>-->
            <div class="row">
                <div class="col-xs-8">
                    <h4 class="panel-title">Посылки</h4>
                </div>
                <div class="col-xs-4">
                    <div class="page-row">
                        <label for="select-count-row" class="panel-title page-row-text">На&nbsp;странице: </label>
                        <select class="page-row-select form-control input-sm" id="select-count-row">
                            <?php for ($i = 20; $i <= 100; $i += 10): ?>
                                <?php
                                if ($i == $dataProvider->pagination->pageSize) {
                                    $selected = ' selected';
                                } else {
                                    $selected = '';
                                }
                                ?>
                                <option value="<?= $i; ?>" <?= $selected ?> ><?= $i; ?>&nbsp;записей</option>
                            <?php endfor; ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <?php require(__DIR__ . '/_panel2.php') ?>
        <div class="panel-body" style="padding: 0 15px; margin-top: 0;">
            <div id="ajaxCrudDatatable">

                <?php
                try {
                    echo GridView::widget([
                        'id' => 'crud-datatable',
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'pjax' => true,
                        'columns' => require(__DIR__ . '/_columns.php'),
                        'panelBeforeTemplate' => '{before}',
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'panel' => [
                            'before' => '<div class="text-right"><p>Всего найдено:&nbsp;<span class="count">' . $count . '</span> на
                    сумму:&nbsp;' . Yii::$app->formatter->asCurrency($sum, 'rub') . '</p></div>',
                            'headingOptions' => ['style' => 'display: none;'],
                            'after' => BulkPackage::widget([
                                    'buttons' =>
                                        Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
                                            ["bulk-delete"],
                                            [
                                                "class" => "btn btn-danger btn-xs",
                                                'role' => 'modal-remote-bulk',
                                                'data-confirm' => false,
                                                'data-method' => false,// for overide yii data api
                                                'data-request-method' => 'post',
                                                'data-confirm-title' => 'Вы уверены?',
                                                'data-confirm-message' => 'Подтвердите удаление выбранных элементов'
                                            ]) . '&nbsp;' .
                                        Html::a('<i class="glyphicon glyphicon-ok"></i>&nbsp;Проверить статус',
                                            ["bulk-check-status"],
                                            [
                                                "class" => "btn btn-primary btn-xs",
                                                'role' => 'modal-remote-bulk',
                                                'data-confirm' => false,
                                                'data-method' => false,// for overide yii data api
                                                'data-request-method' => 'post',
                                                'data-confirm-title' => 'Вы уверены?',
                                                'data-confirm-message' => 'Вы действительно хотите проверить статус у выбранных элементов?'
                                            ]) . '&nbsp;' .
                                        Html::a('<i class="fa fa-envelope"></i>&nbsp;Отправить смс',
                                            ["send-multiply-sms"],
                                            [
                                                'role' => 'modal-remote',
                                                "class" => "bulk-send-btn btn btn-success btn-xs",
                                            ]) . '&nbsp;' .
                                        Html::a('<i class="fa fa-phone"></i>&nbsp;Позвонить',
                                            ["make-multiply-call"],
                                            [
                                                'role' => 'modal-remote',
                                                "class" => "bulk-send-btn btn btn-warning btn-xs",
                                            ]) . '&nbsp;' .
                                        Html::a('<i class="fa fa-sign-out"></i>&nbsp;Экспорт',
                                            ["#"],
                                            [
                                                'id' => 'export-btn',
                                                "class" => "btn btn-info btn-xs",
                                            ]) . '&nbsp;' .
                                        Html::a('<i class="fa fa-commenting"></i>&nbsp;Комментарий',
                                            ['/package/multiply-comment'],
                                            [
                                                'role' => 'modal-remote',
                                                "class" => "bulk-send-btn btn btn-primary btn-xs",
                                            ]) . '&nbsp;' .
                                        Html::a('<i class="fa fa-code-fork"></i>&nbsp;Схема уведомлений',
                                            ['/package/notification-scheme'],
                                            [
                                                'role' => 'modal-remote',
                                                "class" => "bulk-send-btn btn btn-success btn-xs",
                                            ])
                                    ,

                                ])
                                . '<div class="clearfix"></div>',
                        ]
                    ]);
                } catch (Exception $e) {
                    Yii::error($e->getMessage(), '_error');
                    echo $e->getMessage();
                } ?>
            </div>
        </div>
    </div>

<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>

<?php
$script = <<<JS
$(document).ready(function () {
    $(document).on('click', '.bulk-send-btn', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            // Collect all selected ID's
            var selectedIds = getIds();
            if (selectedIds.length === 0) {
                // If no selected ID's show warning
                modal.show();
                modal.setTitle('Ничего не выбрано');
                modal.setContent('Для использования действия выберите одну или несколько строк');
            } else {
                $.post(
                    href,
                    {
                        pks: selectedIds
                    }
                )
            }
        });
    
    $(document).on('click', '#export-btn', function (event) {
            event.preventDefault();
            var href = 'export';
            // Collect all selected ID's
            var selectedIds = getIds();
            if (selectedIds.length === 0) {
                // If no selected ID's show warning
                modal.show();
                modal.setTitle('Ничего не выбрано');
                modal.setContent('Для использования действия выберите одну или несколько строк');
            } else {
                $.post(
                    href,
                    {
                        pks: selectedIds
                    }
                )
            }
        });
    
    function getIds(){
        var selectedIds = [];
            $('input:checkbox[name="selection[]"]').each(function () {
                if (this.checked)
                    selectedIds.push($(this).val());
            });
            console.log(selectedIds);
            return selectedIds;
    }
    
    $(document).on('change', '#select-count-row', function() {
        $.get(
            '/package/set-count-row',
            {count: $(this).val()}
        )
    });
    
    $(document).on('click', '#search-period-btn', function(e) {
        e.preventDefault();
        gogo($(this).attr('href'));
    });
    
    function gogo(href_link){
        var href = href_link;
        var start = $('#date-start').val();
        var end = $('#date-end').val();
        var init = $('#init-day').val();
        var finish = $('#final-day').val();
        var show_comment = Number($('#show-comment').prop('checked'));
        var show_empty_comment = Number($('#show-empty-comment').prop('checked'));
        var params = window.location.search;
        var fields = [
                ['start', start],
                ['end', end],
                ['init_day', init],
                ['final_day', finish],
                ['show_comment', show_comment],
                ['show_empty_comment', show_empty_comment]
             ];
        var arr = params.split('&');

        $.each(fields, function(index, value){
            // console.log("INDEX: " + index + " VALUE: " + value);
            var current_param = 'PackageSearch%5B' + value[0] + '%5D';
            // console.log(params.search(current_param));
            if (params.search(current_param) >= 0){
                $.each(arr, function(sub_index, sub_value) {
                    if (sub_value.search(current_param) >= 0){
                        var parameter = sub_value.split('=');
                        parameter[1] = value[1]; //Присваиваем новое значение
                        arr[index] = parameter.join('=');
                    }
                });
                params = arr.join('&');
            } else {
                params += '&' + current_param + '=' + value[1];
            }
        });
        console.log(params);
        debugger;
        var question = '';
        if (params.indexOf("?") < 0){
            question = "?";
        }
        $.get(
            href + question + params
        )
    }
    
    $(document).on('click', '.clear-btn', function(e) {
        e.preventDefault();
        var id = $(this).find('a').attr('id');
        var href = $(this).find('a').attr('href');
        if (id === 'clear-date-filter'){
            $('#date-start, #date-end').val('');
        } else if (id === 'clear-days-filter'){
            $('#init-day, #final-day').val('');
        }
        gogo(href);
    });
    
    $(document).on('click', '#switch-filter', function() {
        var filter = $('.filter');
        var state = filter.css('display');
        filter.slideToggle(400);
        $.get(
            '/package/save-filter-state',
            {
                state: state
            }
            ,
            function () {
                var warning_filter = $('#warning-filter');
                if (state === 'block'){
                    if ($('#date-start').val() || 
                    $('#date-end').val() || 
                    $('#init-day').val() || 
                    $('#final-day').val()  
                    // !$('#show-empty-comment').attr('checked') ||
                    // !$('#show-comment').attr('checked')
                    ){
                          warning_filter.addClass('fa fa-warning');
                      } else {
                          warning_filter.removeClass('fa fa-warning');
                      }
                } else {
                      warning_filter.removeClass('fa fa-warning');
                }
              
            }
        )
    });
    
    $(document).on('change', '#show-empty-comment, #show-comment', function() {
        var href = '/package/find-by-filter';
        var chk_show_comment = $('#show-comment');
        var chk_show_empty_comment = $('#show-empty-comment');
        if ($(this).attr('id') === 'show-empty-comment'){
            if (chk_show_empty_comment.attr('checked') && chk_show_comment.attr('checked')){
                 chk_show_comment.prop('checked', false);
            }
        } else if ($(this).attr('id') === 'show-comment'){
            if (chk_show_comment.attr('checked') && chk_show_empty_comment.attr('checked')){
                 chk_show_empty_comment.prop('checked', false);
            }
        }
        gogo(href);
        // var show_empty_comment = chk_show_empty_comment.attr('checked');
        // var show_comment = chk_show_comment.attr('checked');
        //  
        // debugger;
        // $.get(
        //     href,
        //     {
        //         start: start,
        //         end: end,
        //         init: init,
        //         finish: finish,
        //         show_empty_comment: show_empty_comment,
        //         show_comment: show_comment
        //     }
        // )
    });
    
    
    $('#export-filter-btn').click(function(e) {
        e.preventDefault();
        $.get(
            $(this).attr('href') +  window.location.search
        )
    });
    
    $(document).on('click', '#filter-check-status', function(e) {
        e.preventDefault();
        switchIcon($(this));
        progress();
    });
    
    function progress() {
        var total = 0;
        var current = 0;
        var btn = $('#filter-check-status');
        var info = $('#progress');
        info.show();
        const evtSource = new EventSource("/package/check-status" +  window.location.search);
        evtSource.addEventListener("progress", function(event) {
                var data = event.data;
                console.log(data);
                if (data.search('total') < 0){
                    current = data.split('=')[1];
                } else {
                    total = data.split('=')[1];
                    if (total == 0){
                        console.log('Нечего обновлять. Закрываемся');
                        evtSource.close();
                    }
                }
                info.html('  ' + current + ' из ' + total);
                if (current == total){
                    if (total != 0){
                        info.html(' Проверка статусов завершена.');
                       setTimeout(function() {
                            location.href = '/package/index' + window.location.search;
                       }, 2000);
                    } else {
                        info.html(' Нет записей для проверки.');
                         setTimeout(function() {
                            info.hide();
                       }, 2000);
                    }
                    evtSource.close();
                    switchIcon(btn);
                    
                    console.log('Закрываемся');
                }
        });
    }
    
    function switchIcon(element) {
        var icon = element.find('i');
        var animate_class = 'fa-cog fa-spin';
        var icon_class = 'fa-repeat'; 
        if (icon.hasClass(icon_class)){
            icon.removeClass(icon_class);
            icon.addClass(animate_class);
        } else {
            icon.removeClass(animate_class);
            icon.addClass(icon_class);
        }
        icon.removeClass('fa-exclamation-triangle');
        console.log(icon);
    }
    
    $(document).on('click', '#start-import-btn-fake', function(e) {
        e.preventDefault();
        var file_export = $('#packageimport-file');
        
        if (file_export.val() === ''){
                   return false;
        }
        $('form').hide();
        $('.info-block').show();
        setTimeout(function(){
            $('#start-import-btn').trigger('click');
        }, 2000);
        
    })
    
});
JS;
$this->registerJs($script);