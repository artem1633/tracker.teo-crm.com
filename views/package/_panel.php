<?php

use kartik\date\DatePicker;
use yii\helpers\Html;

/* @var array $filters Значения фильтров */

Yii::info($filters, 'test');
?>
<div class="panel-before-package">
    <div class="row">
        <div class="col-xs-7">
            <?=
            Html::a('Импорт', ['import'], [
                'role' => 'modal-remote',
                'title' => 'Импортировать из Excel',
                'class' => 'btn btn-info'
            ]) . ' ' .
            Html::a('Добавить <i class="fa fa-plus"></i>', ['create'],
                [
                    'role' => 'modal-remote',
                    'title' => 'Добавить посылку',
                    'class' => 'btn btn-success'
                ]) . '&nbsp;' .
            Html::a('<i class="fa fa-repeat"></i>', ['check-status'],
                [
                    'data-pjax' => 1,
                    'class' => 'btn btn-white',
                    'title' => 'Обновить статусы отправлений'
                ])
            ?>
        </div>
        <div class="col-xs-5">
            <div class="row">
                <div class="col-md-12">
                    <div class="filter-date">
                        <label for=".input-group">Дата&nbsp;отправки</label>
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon">C:</span>
                            <?php
                            try {
                                echo DatePicker::widget([
                                    'name' => 'start-date',
                                    'type' => DatePicker::TYPE_INPUT,
                                    'value' => $filters['date_period']['start'],
                                    'pluginOptions' => [
                                        'todayHighlight' => true,
                                        'autoclose' => true,
                                    ],
                                    'options' => [
                                        'id' => 'date-start',
                                        'class' => 'form-control',
                                        'autocomplete' => "off",
                                    ]
                                ]);
                            } catch (Exception $e) {
                                Yii::error($e->getMessage(), '_error');
                            } ?>
                            <span class="input-group-addon">ПО:</span>
                            <?php
                            try {
                                echo DatePicker::widget([
                                    'name' => 'end-date',
                                    'type' => DatePicker::TYPE_INPUT,
                                    'value' => $filters['date_period']['end'],
                                    'pluginOptions' => [
                                        'todayHighlight' => true,
                                        'autoclose' => true,
                                    ],
                                    'options' => [
                                        'id' => 'date-end',
                                        'class' => 'form-control',
                                        'autocomplete' => "off",
                                    ]
                                ]);
                            } catch (Exception $e) {
                                Yii::error($e->getMessage(), '_error');
                            } ?>
                            <span class="input-group-btn">
                                <?= Html::a('<i class="fa fa-search"></i>', ['/package/find-by-filter'], [
                                    'id' => 'search-period-btn',
                                    'class' => 'btn btn-default'
                                ]) ?>
                            </span>
                            <span class="clear-btn input-group-btn">
                                <?= Html::a('<i class="glyphicon glyphicon-remove"></i>', ['/package/find-by-filter'], [
                                    'id' => 'clear-date-filter',
                                    'title' => 'Очистить фильтр "Дата отправки"',
                                    'class' => 'btn btn-default'
                                ]) ?>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="filter-day-to-back">
                        <label for=".input-group">Дней&nbsp;до&nbsp;возврата</label>
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon">C:</span>
                            <?= Html::input('text', 'init-num-day', $filters['day_period']['start'], [
                                'id' => 'init-day',
                                'class' => 'form-control'
                            ]) ?>
                            <span class="input-group-addon">ПО:</span>
                            <?= Html::input('text', 'final-num-day', $filters['day_period']['end'], [
                                'id' => 'final-day',
                                'class' => 'form-control'
                            ]) ?>
                            <span class="input-group-btn">
                                <?= Html::a('<i class="fa fa-search"></i>', ['/package/find-by-filter'], [
                                    'id' => 'search-period-btn',
                                    'class' => 'btn btn-default'
                                ]) ?>
                            </span>
                            <span class="clear-btn input-group-btn">
                                <?= Html::a('<i class="glyphicon glyphicon-remove"></i>', ['/package/find-by-filter'], [
                                    'id' => 'clear-days-filter',
                                    'title' => 'Очистить фильтр "Дней до возврата"',
                                    'class' => 'btn btn-default'
                                ]) ?>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 text-right" style="padding-top: 5px;">
                    <p>Всего найдено:&nbsp;<span class="count"><?= $count;?></span> на сумму:&nbsp;<?= Yii::$app->formatter->asCurrency($sum, 'rub');?></p>
                </div>
            </div>
        </div>
    </div>
</div>

