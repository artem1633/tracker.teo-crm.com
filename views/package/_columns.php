<?php

use app\models\Package;
use kartik\editable\Editable;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'track_number',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'send_date',
        'format' => ['date', 'php:d M Y'],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'phone',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'name',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'sum',
//        'format' => ['currency', 'rub'],
        'value' => function (Package $model) {
            return Yii::$app->formatter->asCurrency($model->sum, 'rub');
        },
        'format' => 'raw',
        'hAlign' => GridView::ALIGN_CENTER,
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'product_title',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'short_status',
        'filter' => Package::getShortStatusList(),
        'value' => function (Package $model) {
            return Package::getShortStatusById($model->short_status);
        }
    ],
    [
        'class' => '\kartik\grid\EditableColumn',
        'attribute' => 'payed',
        'filter' => [1 => 'Получено', 0 => 'Не получено'],
        'editableOptions' => [
            'formOptions' => ['action' => ['/package/edit']],
            'inputType' => Editable::INPUT_CHECKBOX
        ],
        'value' => function ($model) {
            if ($model->payed) {
                return 'Да';
            }

            return 'Нет';
        },
        'hAlign' => GridView::ALIGN_CENTER,
        'vAlign' => GridView::ALIGN_CENTER,
    ],
//    [
//        'attribute' => 'last_status_updated_at',
//        'format' => 'date',
//        'label' => 'Статус обновлен'
//    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'time_days',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'days_back',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'last_status',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'payed',
    // ],
    [
        'class' => '\kartik\grid\EditableColumn',
        'attribute' => 'comment',
//        'filter' => [
//            'Комментарии' => ['show_comment' => 'Комментарии', 'hide_comment'=> 'Нет комментариев']
//        ],
//        'filter' =>
//            Html::dropDownList('PackageSearch[comment_dd]', Yii::$app->request->get('PackageSearch')['comment_dd'], [
//                0 => '',
//                'show_comment' => 'Комментарии',
//                'hide_comment' => 'Нет комментариев'
//            ], ['class' => 'form-control']) .
//            Html::input('text', 'PackageSearch[comment]', Yii::$app->request->get('PackageSearch')['comment'], [
//                'class' => 'form-control',
//            ]),

        'editableOptions' => [
            'asPopover' => false,
            'formOptions' => ['action' => ['/package/edit']],
            'inputType' => Editable::INPUT_TEXTAREA,
        ],
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created_at',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'template' => '{info} {send-sms} {make-call} {view} {update} {delete}',
        'buttons' => [
            'info' => function ($url, $model) {
                return Html::a('<i class="fa fa-info-circle text-success" style="font-size: 16px;"></i>', $url, [
                    'data-pjax' => 0,
                    'role' => 'modal-remote',
                    'title' => 'Просмотреть информацию'
                ]);
            },
            'send-sms' => function ($url, $model) {
                return Html::a('<i class="fa fa-envelope text-success" style="font-size: 16px;"></i>', $url, [
                    'data-pjax' => 0,
                    'role' => 'modal-remote',
                    'title' => 'Отправить СМС'
                ]);
            },
            'make-call' => function ($url, $model) {
                return Html::a('<i class="fa fa-phone text-warning" style="font-size: 16px;"></i>', $url, [
                    'data-pjax' => 0,
                    'role' => 'modal-remote',
                    'title' => 'Сделать звонок'
                ]);
            },
            'view' => function ($url, $model) {
                return Html::a('<i class="fa fa-eye text-info" style="font-size: 16px;"></i>', $url,
                    ['data-pjax' => 0]);
            },
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role' => 'modal-remote',
                    'title' => 'Удалить',
                    'data-confirm' => false,
                    'data-method' => false,// for overide yii data api
                    'data-request-method' => 'post',
                    'data-confirm-title' => 'Вы уверены?',
                    'data-confirm-message' => 'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                        'role' => 'modal-remote',
                        'title' => 'Изменить',
                        'data-confirm' => false,
                        'data-method' => false,// for overide yii data api
                        'data-request-method' => 'post',
                    ]) . "&nbsp;";
            }
        ],
    ],

];   