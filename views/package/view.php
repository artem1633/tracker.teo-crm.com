<?php

use app\models\OrderEvent;
use app\models\Package;
use app\models\PackagePhonesSms;
use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Package */
/* @var array $dataProvider История отправления */
/* @var PackagePhonesSms $messageDataProvider История Звонков и смс */
/* @var $orderDataProvider OrderEvent История платежей */

$this->title = "Посылка №{$model->id}";
?>
    <div class="package-view">

        <div class="row">
            <!--Статусы-->
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Статусы</h4>
                    </div>
                    <div class="panel-body">
                        <?php
                        try {
                            echo GridView::widget([
                                'id' => 'crud-datatable',
                                'dataProvider' => $dataProvider,
                                'columns' => [
                                    [
                                        'label' => 'Дата',
                                        'attribute' => 'OperationParameters.OperDate',
                                        'format' => ['date', 'php:d M Y H:i'],
                                    ],
                                    [
                                        'label' => 'Индекс',
                                        'attribute' => 'AddressParameters.OperationAddress.Index',
                                    ],
                                    [
                                        'label' => 'ОПС',
                                        'attribute' => 'AddressParameters.OperationAddress.Description',
                                    ],
                                    [
                                        'label' => 'Статус',
                                        'attribute' => 'OperationParameters.OperAttr.Name',
                                        'value' => function ($data) {
                                            $oper_attr_name = $data['OperationParameters']['OperAttr']['Name'] ?? null;
                                            if ($oper_attr_name) {
                                                return $oper_attr_name;
                                            } else {
                                                //Если атрибут не найден - возвращаем тип операции
                                                return $data['OperationParameters']['OperType']['Name'] ?? null;
                                            }
                                        },
                                    ],

                                ],
                                'summary' => false,
                                'striped' => true,
                                'condensed' => true,
                                'responsive' => true,
                            ]);
                        } catch (Exception $e) {
                            Yii::error($e->getMessage(), '_error');
                        } ?>
                    </div>
                </div>
            </div>
            <!--Инфо о наложенном платеже-->
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Информация о наложенном платеже</h4>
                    </div>
                    <div class="panel-body">
                        <?php
                        try {
                            echo GridView::widget([
                                'id' => 'crud-datatable',
                                'dataProvider' => $orderDataProvider,
//                                'filterModel' => $orderSearchModel,
                                'pjax' => true,
                                'columns' => [
                                    'number',
                                    'name',
                                    'date:datetime',
                                    [
                                        'attribute' => 'sum',
                                        'label' => 'Сумма (р.)',
                                        'value' => function (OrderEvent $model) {
                                            return Yii::$app->formatter->asCurrency($model->sum / 100, 'rub') ?? null;
                                        },
                                        'format' => 'raw',

                                    ],
                                    'index',
                                    'index_to',
                                ],
                                'striped' => true,
                                'condensed' => true,
                                'responsive' => true,
                                'summary' => false,
                            ]);
                        } catch (Exception $e) {
                            Yii::error($e->getMessage(), '_error');
                            echo $e->getMessage();
                        } ?>
                    </div>
                </div>
            </div>
            <!--СМС и Звонки-->
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Информация по СМС и звонкам</h4>
                    </div>
                    <div class="panel-body">
                        <?php
                        try {
                            echo GridView::widget([
                                'id' => 'crud-data-table-sms-info',
                                'dataProvider' => $messageDataProvider,
                                'columns' => [
                                    [
                                        'class' => 'kartik\grid\SerialColumn',
                                        'width' => '30px',
                                    ],
                                    [
                                        'class' => '\kartik\grid\DataColumn',
                                        'attribute' => 'type',
                                        'value' => function (PackagePhonesSms $model) {
                                            if ($model->sms_id ?? '') {
                                                return 'СМС';
                                            } elseif ($model->call_id ?? '') {
                                                return 'Тел. звонок';
                                            }
                                            return null;
                                        },
                                        'label' => 'Тип',
                                    ],
                                    'phone',
                                    'text',
                                    [
                                        'class' => '\kartik\grid\DataColumn',
                                        'attribute' => 'status',
                                        'value' => function (PackagePhonesSms $model) {
                                            if ($model->status) {
                                                if ($model->call_id) {
                                                    return $model->getCallStatusName($model->status) ?? null;
                                                } elseif ($model->sms_id) {
                                                    return $model->getSmsStatusName($model->status) ?? null;
                                                }
                                            }
                                            return null;
                                        }
                                    ],
                                    [
                                        'attribute' => 'datetime_deliver',
                                        'format' => ['date', 'php:d M Y H:i'],
                                        'value' => function(PackagePhonesSms $model){
                                            if (!$model->datetime_deliver || $model->datetime_send == ''){
                                                return $model->datetime_send;
                                            } elseif (!$model->datetime_deliver && !$model->datetime_send == ''){
                                                return $model->created_at;
                                            }
                                            return $model->datetime_deliver;
                                        }
                                    ],
                                ],
                                'summary' => false,
                                'striped' => true,
                                'condensed' => true,
                                'responsive' => true,
                            ]);
                        } catch (Exception $e) {
                            Yii::error($e->getMessage(), 'error');
                        }
                        ?>
                    </div>
                </div>
            </div>
            <!--Информация-->
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">Информация</h4>
                    </div>
                    <div class="panel-body">
                        <?php
                        try {
                            echo DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'id',
                                    'track_number',
                                    [
                                        'attribute' => 'send_date',
                                        'format' => ['date', 'php:d M Y'],
                                    ],
                                    'phone',
                                    'name',
                                    [
                                        'attribute' => 'sum',
                                        'value' => Yii::$app->formatter->asCurrency($model->sum, 'rub'),
                                        'format' => 'raw',

                                    ],
                                    'product_title',
                                    'time_days',
                                    'days_back',
                                    [
                                        'attribute' => 'status',
                                        'value' => Package::getShortStatusById($model->short_status),
                                    ],
                                    [
                                        'attribute' => 'scheme_id',
                                        'value' => $model->scheme->name ?? '',
                                    ],
                                    [
                                        'attribute' => 'status',
                                        'label' => 'Последний статус'
                                    ],
                                    [
                                        'attribute' => 'payed',
                                        'value' => function (Package $model) {
                                            if ($model->payed) {
                                                $text = 'Да';
                                                $class = 'change-payed btn btn-success';
                                            } else {
                                                $text = 'Нет';
                                                $class = 'change-payed btn btn-warning';
                                            }
                                            return Html::button($text, [
                                                'id' => $model->id,
                                                'class' => $class
                                            ]);
                                        },
                                        'format' => 'raw',
                                    ],
                                    [
                                        'attribute' => 'payed_date',
                                        'format' => ['date', 'php:d M Y H:i'],
                                        'contentOptions' => [
                                            'id' => 'date-payed'
                                        ]

                                    ],
                                    'comment:ntext',
                                    [
                                        'attribute' => 'created_at',
                                        'format' => ['date', 'php:d M Y H:i'],
                                    ],
                                    [
                                        'attribute' => 'last_status_updated_at',
                                        'format' => ['date', 'php:d M Y H:i'],
                                    ],
                                ],
                            ]);
                        } catch (Exception $e) {
                            Yii::error($e->getMessage(), '_error');
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
$script = <<<JS
$(document).ready(function(){
    $(document).on('click', '.change-payed', function() {
        var btn = $(this);
        var d_payed = $('#date-payed');
        var id = btn.attr('id');
        $.get(
            '/package/change-payed',
            {id: id},
            function(response) {
                if (response['success'] === 1){
                    if (response['data'] === 1){
                        btn.removeClass('btn-warning');
                        btn.addClass('btn-success');
                        btn.html('Да');
                        d_payed.html(response['date_payed'])
                    } else {
                        btn.removeClass('btn-success');
                        btn.addClass('btn-warning');
                        btn.html('Нет');
                        d_payed.html('');
                    }
                } else {
                    alert(response['data']);
                }
              
            }
        )
    })
})
JS;
$this->registerJs($script);
