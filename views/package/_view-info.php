<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PackageInfo */
/* @var $pkg_model app\models\Package */

$this->title = "Посылка №{$model->id}";
?>
<div class="package-view">

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Информация</h4>
                </div>
                <div class="panel-body">
                    <?php
                    try {
                        echo DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'type',
                                [
                                    'attribute' => 'recipient',
                                    'value' => $pkg_model->recipient,
                                    'label' => 'Получатель',
                                ],
                                [
                                    'attribute' => 'sender',
                                    'value' => $pkg_model->sender,
                                    'label' => 'Отправитель',
                                ],
                                'weight',
                                [
                                    'attribute' => 'cost',
                                    'format' => ['currency', 'rub'],
                                ],
                                [
                                    'attribute' => 'pay_on_delivery',
                                    'format' => ['currency', 'rub'],
                                ],
                                [
                                    'attribute' => 'days_back',
                                    'label' => 'Дней до возврата',
                                    'value' => $pkg_model->days_back,

                                ],
                                [
                                    'attribute' => 'time_days',
                                    'label' => 'Дней в пути',
                                    'value' => $pkg_model->time_days,
                                ],
                                [
                                    'attribute' => 'index_recipient',
                                    'value' => $pkg_model->destination_zip,
                                    'label' => 'Индекс адресата',
                                ],
                                [
                                    'attribute' => 'address_recipient',
                                    'value' => $pkg_model->destination_post_address,
                                    'label' => 'ОПС адресата',
                                ],
                            ],
                        ]);
                    } catch (Exception $e) {
                        Yii::error($e->getMessage(), '_error');
                    } ?>
                </div>
            </div>
        </div>
    </div>
</div>
