<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Package */

?>
<div class="package-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
