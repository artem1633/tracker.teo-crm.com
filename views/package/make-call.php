<?php

use app\models\SmsTemplate;
use yii\widgets\ActiveForm;

/**
 * @var \app\models\forms\MakeCallForm $model
 */

?>

<?php $form = ActiveForm::begin() ?>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'templateId')->dropDownList(SmsTemplate::getList(SmsTemplate::TEMPLATE_TYPE_CALL)) ?>
    </div>
</div>

<?php ActiveForm::end() ?>
