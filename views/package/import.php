<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $model \app\models\forms\PackageImport
 */

?>
<div class="info-block" style="display: none;">
<div class="info" style="display:flex; align-items: flex-end;">
    <div class="animate-spin">
        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
    </div>
    <div class="information">
        <p>&nbsp;Импорт информации об отправлениях, ожидайте...</p>
    </div>
</div>
</div>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'file')->fileInput() ?>
    </div>
</div>
<div class="text-right">
    <?= Html::button('Начать импорт', [
        'id' => 'start-import-btn-fake',
        'class' => 'btn btn-primary',
    ])
    ?>
</div>

<?php ActiveForm::end() ?>


