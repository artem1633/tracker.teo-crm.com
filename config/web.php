<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'app\bootstrap\AppBootstrap'],
    'defaultRoute' => 'user',
    'timeZone' => 'UTC',
//    'timeZone' => 'Europe/Moscow',
    'language' => 'ru-RU',
    'name' => 'CRM',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'GhAcZ2j2hHCv9-XMRK1mi0wYRu29SWwu',
        ],
        'smsService' => [
            'class' => 'app\services\SmsTelService',
        ],
        'voiceService' => [
            'class' => 'app\services\VoiceTelService',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'rules' => [

            ],
        ],
        'formatter' => [
            'timeZone' => 'Europe/Moscow',
            'locale' => 'ru-RU',
            'dateFormat' => 'dd.mm.yyyy',
            'decimalSeparator' => ',',
            'thousandSeparator' => '&nbsp;',
            'currencyCode' => '₽',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['_error'],
                    'logFile' => '@app/runtime/logs/_error.log'
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['test'],
                    'logFile' => '@app/runtime/logs/test.log'
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'modules' => [
        'api' => [
            'class' => 'app\modules\api\Api'
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ]
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'generators' => [
            'crud' => [
                'class' => 'yii\gii\generators\crud\Generator',
                'templates' => ['My' => '@app/vendor/yiisoft/yii2-gii/generators/crud/admincolor']
            ]
        ],
        'allowedIPs' => ['*'],
    ];
}

return $config;
